FROM node:12.16.3-alpine3.9
RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app
COPY . .
EXPOSE 3000
CMD ["yarn", "start-service","--service-name=users"]