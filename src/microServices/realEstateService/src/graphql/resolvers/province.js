import DistrictModel from '../../models/district';

export async function listDistrictsResolver(province) {
	try {
		const { districts: districtIds } = province;
		return DistrictModel.find({
      _id: districtIds 
    });
	} catch (e) {
		return Promise.reject(e);
	}
}