import ProvinceModel from '../../models/province';
import WardModel from '../../models/ward';

export async function provinceResolver(district) {
	try {
		const { province: provinceId } = district;
		return ProvinceModel.findOne({ _id: provinceId, deleteAt: null });
	} catch (e) {
		return Promise.reject(e);
	}
}

export async function getWardsOfDistrictResolver(district) {
	try {
		const { wards: wardIds } = district;
		return WardModel.find({ _id: wardIds, deleteAt: null });
	} catch (e) {
		return Promise.reject(e);
	}
}
