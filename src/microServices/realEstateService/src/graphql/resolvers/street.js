import DistrictModel from '../../models/district';
import ProvinceModel from '../../models/province';
import WardModel from '../../models/ward';


export async function streetDistrictResolver(street) {
	try {
		const { district: districtId } = street;
		return DistrictModel.findOne({ _id: districtId, deleteAt: null });
	} catch (e) {
		return Promise.reject(e);
	}
}

export async function streetProvinceResolver(street) {
	try {
		const { province: provinceId } = street;
		return ProvinceModel.findOne({ _id: provinceId, deleteAt: null });
	} catch (e) {
		return Promise.reject(e);
	}
}

export async function streetWardResolver(street) {
	try {
		const { ward: wardId } = street;
		return WardModel.findOne({ _id: wardId, deleteAt: null });
	} catch (e) {
		return Promise.reject(e);
	}
}