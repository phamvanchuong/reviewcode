import DistrictModel from '../../models/district';
import StreetModel from '../../models/street';


export async function wardResolver(ward) {
	try {
		const { district: districtId } = ward;
		return DistrictModel.findOne({ _id: districtId, deleteAt: null });
	} catch (e) {
		return Promise.reject(e);
	}
}

export async function streetResolver(ward) {
	try {
		const { streets: streetIds } = ward;
		return StreetModel.find({ _id: streetIds, deleteAt: null });
	} catch (e) {
		return Promise.reject(e);
	}
}

