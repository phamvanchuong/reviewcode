import { Primitives } from '../../../../../utils';
import DistrictModel from '../../models/district';
import { RealEstateMessageType, DistrictType } from '../types';
import { Types } from 'mongoose';
import moment from 'moment';
import ProvinceModel from '../../models/province';

export const createDistrict = {
	type: DistrictType,
	description: 'Create new district',
	args: {
		name: Primitives.requiredString(),
		shortname: Primitives.string(),
		provinceId: Primitives.requiredString()
	},
	resolve: async (rootValue, { name, shortname, provinceId }, { req }) => {
		try {
			const foundDistrict = await DistrictModel.findOne({ name: name, province: Types.ObjectId(provinceId), deletedAt: null}),
			foundProvince = await ProvinceModel.findOne({ _id: provinceId, deletedAt: null});
      
      if (foundDistrict) {
        return Promise.reject(new Error(req.t('duplicateDistrict')));
			}
			if (!foundProvince) {
				return Promise.reject(new Error(req.t('notFoundProvince')));
			}
			const newDistrict = await new DistrictModel({ name, shortname, province: provinceId }).save();
			foundProvince.districts.push(newDistrict.id);
			await foundProvince.save();
			return newDistrict;
		} catch (e) {
			return Promise.reject(e);
		}
	}
};

export const updateDistrict = {
	type: DistrictType,
	description: 'Update existing district',
	args: {
    districtId: Primitives.requiredString(),
		name: Primitives.string(),
		shortname: Primitives.string(),
		provinceId: Primitives.requiredString()
	},
	resolve: async (rootValue, { districtId, name, shortname, provinceId }, { req }) => {
		try {
			const foundDistrict = await DistrictModel.findOne({ _id: districtId, deletedAt: null}),
			foundProvince = await ProvinceModel.findOne({ _id: provinceId, deletedAt: null}),
				payload = {};
      
      if (!foundDistrict) {
        return Promise.reject(new Error(req.t('districtNotFound')));
			}
			if (!foundProvince) {
        return Promise.reject(new Error(req.t('notFoundProvince')));
			}
			if (name && foundDistrict.province && (foundProvince._id.toString() !== foundDistrict.province.toString())) {
				const duplicatedDistrict = await DistrictModel.findOne({ name: name, province: Types.ObjectId(provinceId), deletedAt: null});
				if (duplicatedDistrict) {
					return Promise.reject(new Error(req.t('duplicateDistrict')));
				}
			}
      
      let updateObj = { name, shortname };
			Object.entries(updateObj).forEach(([key, value]) => {
				if (value && !payload[key]) {
					payload[key] = value
				}
			});
			payload.province = provinceId;
			if (foundDistrict.province && (foundProvince._id.toString() !== foundDistrict.province.toString())) {
				const removeDistProvince = await ProvinceModel.findOne({_id: foundDistrict.province, deletedAt: null});
				if (!removeDistProvince) {
					return Promise.reject(new Error(req.t('notFoundProvince')));
				}
				const index = removeDistProvince.districts.indexOf(foundDistrict._id);
				if (index !== -1) {
					removeDistProvince.districts.splice(index, 1);
					await removeDistProvince.save();
				}
				foundProvince.districts.push(foundDistrict._id);
				await foundProvince.save();
			}
      return await DistrictModel.findByIdAndUpdate(foundDistrict._id, payload);
		} catch (e) {
			return Promise.reject(e);
		}
	}
};

export const deleteDistrict = {
	type: DistrictType,
	description: 'Delete an existing district',
	args: {
    districtId: Primitives.requiredString()
	},
	resolve: async (rootValue, { districtId }, { req }) => {
		try {
      const foundDistrict = await DistrictModel.findOne({ _id: districtId, deletedAt: null});
      
      if (!foundDistrict) {
        return Promise.reject(new Error(req.t('notFoundDistrict')));
      }
      // Find real estate using district
      //const usingRealState = await RealEstate.findOne({ district: Types.ObjectId(districtId), deletedAt: null});
      
      // if (usingRealState) {
      //   return Promise.reject(new Error(req.t('districtInUse')));
      // }
      foundDistrict.deletedAt =  moment();
      await foundDistrict.save();
      return foundDistrict;
		} catch (e) {
			return Promise.reject(e);
		}
	}
};