import { Primitives } from '../../../../../utils';
import ProvinceModel from '../../models/province';
import { RealEstateMessageType, ProvinceType } from '../types';
import { Types } from 'mongoose';
import moment from 'moment';

export const createProvince = {
	type: ProvinceType,
	description: 'Create new province',
	args: {
		name: Primitives.requiredString(),
		shortname: Primitives.string()
	},
	resolve: async (rootValue, { name, shortname }, { req }) => {
		try {
      const foundProvince = await ProvinceModel.findOne({ name: name, deletedAt: null});
      
      if (foundProvince) {
        return Promise.reject(new Error(req.t('duplicateProvince')));
      }
      return await new ProvinceModel({ name, shortname }).save();
		} catch (e) {
			return Promise.reject(e);
		}
	}
};

export const updateProvince = {
	type: ProvinceType,
	description: 'Update existing province',
	args: {
    provinceId: Primitives.requiredString(),
		name: Primitives.string(),
		shortname: Primitives.string()
	},
	resolve: async (rootValue, { provinceId, name, shortname }, { req }) => {
		try {
      const foundProvince = await ProvinceModel.findOne({ _id: provinceId, deletedAt: null}),
				payload = {};
      
      if (!foundProvince) {
        return Promise.reject(new Error(req.t('notFoundProvince')));
      }
      const duplicatedProvince = await ProvinceModel.findOne({ name: name, deletedAt: null});
      
      if (duplicatedProvince) {
        return Promise.reject(new Error(req.t('duplicateProvince')));
      }
      let updateObj = { name, shortname };
			Object.entries(updateObj).forEach(([key, value]) => {
				if (value && !payload[key]) {
					payload[key] = value
				}
			});
      return await ProvinceModel.findByIdAndUpdate(foundProvince._id, payload);
		} catch (e) {
			return Promise.reject(e);
		}
	}
};

export const deleteProvince = {
	type: ProvinceType,
	description: 'Delete an existing province',
	args: {
    provinceId: Primitives.requiredString()
	},
	resolve: async (rootValue, { provinceId, name, shortname }, { req }) => {
		try {
      const foundProvince = await ProvinceModel.findOne({ _id: provinceId, deletedAt: null}),
				payload = {};
      
      if (!foundProvince) {
        return Promise.reject(new Error(req.t('notFoundProvince')));
      }
      // Find real estate using province
      //const usingRealState = await RealEstate.findOne({ province: Types.ObjectId(provinceId), deletedAt: null});
      
      // if (usingRealState) {
      //   return Promise.reject(new Error(req.t('provinceInUse')));
      // }
      foundProvince.deletedAt =  moment();
      await foundProvince.save();
      return foundProvince;
		} catch (e) {
			return Promise.reject(e);
		}
	}
};