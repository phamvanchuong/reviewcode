import { GraphQLObjectType } from 'graphql';
import { GraphQLString as String } from 'graphql/type/scalars';
import { createProvince, updateProvince, deleteProvince } from './province';
import { createDistrict, updateDistrict, deleteDistrict } from './district';
import { createWard, updateWard, deleteWard } from './ward';
import { createStreet, updateStreet, deleteStreet } from './street';

export default new GraphQLObjectType({
	name: 'Mutations',
	fields: () => ({
		[`${process.env.SERVICE_NAME}Greeting`]: greeting,
		createProvince,
		updateProvince,
		deleteProvince,
		createDistrict,
		updateDistrict,
		deleteDistrict,
		createWard,
		updateWard,
		deleteWard,
		createStreet,
		updateStreet,
		deleteStreet
	}),
});

const greeting = {
	type: String,
	description: 'A warm welcome message from GraphQL, usually used to Test if the system working..',
	resolve: (rootValue, params, context) => {
		const { req } = context;
		return req.t('greetingMutation', { service: process.env.SERVICE_NAME });
	},
};
