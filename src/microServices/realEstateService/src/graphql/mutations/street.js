import { Primitives } from '../../../../../utils';
import StreetModel from '../../models/street';
import { StreetType } from '../types';
import { Types } from 'mongoose';
import moment from 'moment';
import DistrictModel from '../../models/district';
import WardModel from '../../models/ward';

export const createStreet = {
	type: StreetType,
	description: 'Create new street',
	args: {
		name: Primitives.requiredString(),
    shortname: Primitives.string(),
    isShoppingArea: Primitives.boolean(),
    isEntertainmentArea: Primitives.boolean(),
    isResidentialArea: Primitives.boolean(),
    isAdministrativeArea: Primitives.boolean(),
    isFoodnDrinkArea: Primitives.boolean(),
    isNearSchool: Primitives.boolean(),
    wardId: Primitives.requiredString()
	},
	resolve: async (rootValue, { 
    name, shortname,
    isShoppingArea,
    isEntertainmentArea,
    isResidentialArea,
    isAdministrativeArea,
    isFoodnDrinkArea,
    isNearSchool,
    wardId}, { req }) => {
		try {
			const foundStreet = await StreetModel.findOne({ name: name, ward: Types.ObjectId(wardId), deletedAt: null}),
      foundWard = await WardModel.findOne({ _id: wardId, deletedAt: null});
      
      if (foundStreet) {
        return Promise.reject(new Error(req.t('duplicateStreet')));
			}
			if (!foundWard) {
				return Promise.reject(new Error(req.t('districtNotFound')));
      }
      const foundDistrict = await DistrictModel.findOne({ _id: foundWard.district, deletedAt: null})
			const newStreet = await new StreetModel({ 
        name, shortname, 
        isShoppingArea,
        isEntertainmentArea,
        isResidentialArea,
        isAdministrativeArea,
        isFoodnDrinkArea,
        isNearSchool,
        ward: wardId, 
        district: foundWard.district, 
        province: foundDistrict.province }).save();
			foundWard.streets.push(newStreet.id);
			await foundWard.save();
			return newStreet;
		} catch (e) {
			return Promise.reject(e);
		}
	}
};

export const updateStreet = {
	type: StreetType,
	description: 'Update existing street',
	args: {
    streetId: Primitives.requiredString(),
    wardId: Primitives.string(),
    name: Primitives.string(),
    isShoppingArea: Primitives.boolean(),
    isEntertainmentArea: Primitives.boolean(),
    isResidentialArea: Primitives.boolean(),
    isAdministrativeArea: Primitives.boolean(),
    isFoodnDrinkArea: Primitives.boolean(),
    isNearSchool: Primitives.boolean(),
		shortname: Primitives.string()
	},
  resolve: async (rootValue, { 
      streetId, wardId,
      name, shortname,
      isShoppingArea,
      isEntertainmentArea,
      isResidentialArea,
      isAdministrativeArea,
      isFoodnDrinkArea,
      isNearSchool,
    }, { req }) => {
		try {
			const foundStreet = await StreetModel.findOne({ _id: streetId, deletedAt: null}),
				payload = {};
      
      if (!foundStreet) {
        return Promise.reject(new Error(req.t('streetNotFound')));
			}
			if (wardId) {
				const foundWard = await WardModel.findOne({ _id: wardId, deletedAt: null});
				if (!foundWard) {
					return Promise.reject(new Error(req.t('wardNotFound')));
				}
				if (name && foundStreet.ward && (foundWard._id.toString() !== foundStreet.ward.toString())) {
					const duplicatedStreet = await StreetModel.findOne({ name: name, ward: Types.ObjectId(wardId), deletedAt: null});
					if (duplicatedStreet) {
						return Promise.reject(new Error(req.t('duplicateStreet')));
					}
					const removeStreetOfWard = await WardModel.findOne({_id: foundStreet.ward, deletedAt: null});
					if (!removeStreetOfWard) {
						return Promise.reject(new Error(req.t('wardNotFound')));
					}
					const index = removeStreetOfWard.streets.indexOf(foundStreet._id);
					if (index !== -1) {
						removeStreetOfWard.streets.splice(index, 1);
						await removeStreetOfWard.save();
					}
					foundWard.streets.push(foundStreet._id);
					await foundWard.save();
					payload.ward = wardId;
				}
			}
      
      let updateObj = { 
        name, shortname, 
        isShoppingArea,
        isEntertainmentArea,
        isResidentialArea,
        isAdministrativeArea,
        isFoodnDrinkArea,
        isNearSchool };
			Object.entries(updateObj).forEach(([key, value]) => {
				if (value && !payload[key]) {
					payload[key] = value
				}
			});
      return await StreetModel.findByIdAndUpdate(foundStreet._id, payload);
		} catch (e) {
			return Promise.reject(e);
		}
	}
};

export const deleteStreet = {
	type: StreetType,
	description: 'Delete an existing street',
	args: {
    streetId: Primitives.requiredString()
	},
	resolve: async (rootValue, { streetId }, { req }) => {
		try {
      const foundStreet = await StreetModel.findOne({ _id: streetId, deletedAt: null});
      
      if (!foundStreet) {
        return Promise.reject(new Error(req.t('streetNotFound')));
      }
      // Find real estate using street
      //const usingRealState = await RealEstate.findOne({ street: Types.ObjectId(streetId), deletedAt: null});
      
      // if (usingRealState) {
      //   return Promise.reject(new Error(req.t('streetInUse')));
      // }
      return StreetModel.findByIdAndUpdate(foundStreet._id, { deletedAt: moment()});
		} catch (e) {
			return Promise.reject(e);
		}
	}
};