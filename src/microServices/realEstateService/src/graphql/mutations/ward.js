import { Primitives } from '../../../../../utils';
import WardModel from '../../models/ward';
import { WardType } from '../types';
import { Types } from 'mongoose';
import moment from 'moment';
import DistrictModel from '../../models/district';

export const createWard = {
	type: WardType,
	description: 'Create new ward',
	args: {
		name: Primitives.requiredString(),
		shortname: Primitives.string(),
		districtId: Primitives.requiredString()
	},
	resolve: async (rootValue, { name, shortname, districtId }, { req }) => {
		try {
			const foundWard = await WardModel.findOne({ name: name, district: Types.ObjectId(districtId), deletedAt: null}),
			foundDistrict = await DistrictModel.findOne({ _id: districtId, deletedAt: null});
      
      if (foundWard) {
        return Promise.reject(new Error(req.t('duplicateWard')));
			}
			if (!foundDistrict) {
				return Promise.reject(new Error(req.t('districtNotFound')));
			}
			const newWard = await new WardModel({ name, shortname, district: districtId }).save();
			foundDistrict.wards.push(newWard.id);
			await foundDistrict.save();
			return newWard;
		} catch (e) {
			return Promise.reject(e);
		}
	}
};

export const updateWard = {
	type: WardType,
	description: 'Update existing ward',
	args: {
    wardId: Primitives.requiredString(),
		name: Primitives.string(),
		shortname: Primitives.string(),
		districtId: Primitives.string()
	},
	resolve: async (rootValue, { wardId, name, shortname, districtId }, { req }) => {
		try {
			const foundWard = await WardModel.findOne({ _id: wardId, deletedAt: null}),
				payload = {};
      
      if (!foundWard) {
        return Promise.reject(new Error(req.t('wardNotFound')));
			}
			if (districtId) {
				const foundDistrict = await DistrictModel.findOne({ _id: districtId, deletedAt: null});
				if (!foundDistrict) {
					return Promise.reject(new Error(req.t('districtNotFound')));
				}
				if (name && foundWard.district && (foundDistrict._id.toString() !== foundWard.district.toString())) {
					const duplicatedWard = await WardModel.findOne({ name: name, district: Types.ObjectId(districtId), deletedAt: null});
					if (duplicatedWard) {
						return Promise.reject(new Error(req.t('duplicateWard')));
					}
					const removeWardOfDistrict = await DistrictModel.findOne({_id: foundWard.district, deletedAt: null});
					if (!removeWardOfDistrict) {
						return Promise.reject(new Error(req.t('districtNotFound')));
					}
					const index = removeWardOfDistrict.wards.indexOf(foundWard._id);
					if (index !== -1) {
						removeWardOfDistrict.wards.splice(index, 1);
						await removeWardOfDistrict.save();
					}
					foundDistrict.wards.push(foundWard._id);
					await foundDistrict.save();
					payload.district = districtId;
				}
			}
      
      let updateObj = { name, shortname };
			Object.entries(updateObj).forEach(([key, value]) => {
				if (value && !payload[key]) {
					payload[key] = value
				}
			});
      return await WardModel.findByIdAndUpdate(foundWard._id, payload);
		} catch (e) {
			return Promise.reject(e);
		}
	}
};

export const deleteWard = {
	type: WardType,
	description: 'Delete an existing ward',
	args: {
    wardId: Primitives.requiredString()
	},
	resolve: async (rootValue, { wardId }, { req }) => {
		try {
      const foundWard = await WardModel.findOne({ _id: wardId, deletedAt: null});
      
      if (!foundWard) {
        return Promise.reject(new Error(req.t('wardNotFound')));
      }
      // Find real estate using ward
      //const usingRealState = await RealEstate.findOne({ Ward: Types.ObjectId(wardId), deletedAt: null});
      
      // if (usingRealState) {
      //   return Promise.reject(new Error(req.t('wardInUse')));
      // }
      foundWard.deletedAt =  moment();
      await foundWard.save();
      return foundWard;
		} catch (e) {
			return Promise.reject(e);
		}
	}
};