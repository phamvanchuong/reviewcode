import { Primitives } from '../../../../../utils';
import WardModel from '../../models/ward';
import { StreetType, ListStreetsType } from '../types';
import { Types } from 'mongoose';
import { GraphQLString } from 'graphql';
import StreetModel from '../../models/street';

export const getListStreetsOfWard = {
	type: ListStreetsType,
	description: 'Get list streets of a specific ward',
	args: {
		wardIds: Primitives.requiredList(GraphQLString),
		name: Primitives.string(),
		limit: Primitives.int(),
		page: Primitives.int()
	},
	resolve: async (rootValue, { wardIds, name, limit, page }, { req }) => {
		try {
			const query = name ? { $text: { $search: name }} : {},
				options = {
					page: page || 1,
					limit: limit || 10,
					customLabels: {
						totalDocs: 'total',
						docs: 'streets',
						perPage: 'limit',
						currentPage: 'page',
						next: 'nextPage',
						prev: 'prevPage',
						pageCount: 'totalPages',
						slNo: 'pagingCounter'
					}
				};
			if (wardIds.length) {
				query.ward = { $in: wardIds.map(ward => { return new Types.ObjectId(ward) }) };
			}
			return await StreetModel.paginate(query, options);
		} catch (e) {
			return Promise.reject(e);
		}
	}
};

export const getStreetDetails = {
	type: StreetType,
	description: 'Get street details',
	args: {
		streetId: Primitives.requiredString()
	},
	resolve: async (rootValue, { streetId }, { req }) => {
		try {
      const foundStreet = await StreetModel.findOne({ _id: streetId, deletedAt: null});
      
      if (!foundStreet) {
        return Promise.reject(new Error(req.t('streetNotFound')));
      }
      return foundStreet;
		} catch (e) {
			return Promise.reject(e);
		}
	}
};