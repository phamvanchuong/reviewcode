import { Primitives } from '../../../../../utils';
import ProvinceModel from '../../models/province';
import { RealEstateMessageType, ProvinceType, ListProvincesType } from '../types';

export const getListProvinces = {
	type: ListProvincesType,
	description: 'Get list of provinces',
	args: {
		name: Primitives.string(),
		limit: Primitives.int(),
		page: Primitives.int()
	},
	resolve: async (rootValue, { name, limit, page }, { req }) => {
		try {
      const query = name ? { $text: { $search: name }} : {},
				options = {
					page: page || 1,
					limit: limit || 10,
					customLabels: {
						totalDocs: 'total',
						docs: 'provinces',
						perPage: 'limit',
						currentPage: 'page',
						next: 'nextPage',
						prev: 'prevPage',
						pageCount: 'totalPages',
						slNo: 'pagingCounter'
					}
				};
			return await ProvinceModel.paginate(query, options);
		} catch (e) {
			return Promise.reject(e);
		}
	}
};

export const getProvinceDetails = {
	type: ProvinceType,
	description: 'Get province details',
	args: {
		provinceId: Primitives.requiredString()
	},
	resolve: async (rootValue, { provinceId }, { req }) => {
		try {
      const foundProvince = await ProvinceModel.findOne({ _id: provinceId, deletedAt: null});
      
      if (!foundProvince) {
        return Promise.reject(new Error(req.t('notFoundProvince')));
      }
      return foundProvince;
		} catch (e) {
			return Promise.reject(e);
		}
	}
};