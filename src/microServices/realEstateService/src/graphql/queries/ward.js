import { Primitives } from '../../../../../utils';
import DistrictModel from '../../models/district';
import WardModel from '../../models/ward';
import { WardType, ListWardsType } from '../types';
import { Types } from 'mongoose';

export const getListWardsOfDistrict = {
	type: ListWardsType,
	description: 'Get list of wards of a specific district',
	args: {
		districtId: Primitives.requiredString(),
		name: Primitives.string(),
		limit: Primitives.int(),
		page: Primitives.int()
	},
	resolve: async (rootValue, { districtId, name, limit, page }, { req }) => {
		try {
			const query = name ? { $text: { $search: name }} : {},
				foundDistrict = await DistrictModel.findOne({ _id: districtId, deletedAt: null }),
				options = {
					page: page || 1,
					limit: limit || 10,
					customLabels: {
						totalDocs: 'total',
						docs: 'wards',
						perPage: 'limit',
						currentPage: 'page',
						next: 'nextPage',
						prev: 'prevPage',
						pageCount: 'totalPages',
						slNo: 'pagingCounter'
					}
				};

			if (!foundDistrict) {
				return Promise.reject(new Error(req.t('districtNotFound')));
			}
			query.district = Types.ObjectId(districtId);
			return await WardModel.paginate(query, options);
		} catch (e) {
			return Promise.reject(e);
		}
	}
};

export const getWardDetails = {
	type: WardType,
	description: 'Get ward details',
	args: {
		wardId: Primitives.requiredString()
	},
	resolve: async (rootValue, { wardId }, { req }) => {
		try {
      const foundWard = await WardModel.findOne({ _id: wardId, deletedAt: null});
      
      if (!foundWard) {
        return Promise.reject(new Error(req.t('wardNotFound')));
      }
      return foundWard;
		} catch (e) {
			return Promise.reject(e);
		}
	}
};