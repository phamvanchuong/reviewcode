import { Primitives } from '../../../../../utils';
import DistrictModel from '../../models/district';
import ProvinceModel from '../../models/province';
import { DistrictType, ListDistrictsType } from '../types';
import { Types } from 'mongoose';

export const getListDistrictsOfProvince = {
	type: ListDistrictsType,
	description: 'Get list of districts of a specific province',
	args: {
		provinceId: Primitives.requiredString(),
		name: Primitives.string(),
		limit: Primitives.int(),
		page: Primitives.int()
	},
	resolve: async (rootValue, { provinceId, name, limit, page }, { req }) => {
		try {
			const query = name ? { $text: { $search: name }} : {},
				foundProvince = await ProvinceModel.findOne({ _id: provinceId, deletedAt: null }),
				options = {
					page: page || 1,
					limit: limit || 10,
					customLabels: {
						totalDocs: 'total',
						docs: 'districts',
						perPage: 'limit',
						currentPage: 'page',
						next: 'nextPage',
						prev: 'prevPage',
						pageCount: 'totalPages',
						slNo: 'pagingCounter'
					}
				};

			if (!foundProvince) {
				return Promise.reject(new Error(req.t('notFoundProvince')));
			}
			query.province = Types.ObjectId(provinceId);
			return await DistrictModel.paginate(query, options);
		} catch (e) {
			return Promise.reject(e);
		}
	}
};

export const getDistrictDetails = {
	type: DistrictType,
	description: 'Get district details',
	args: {
		districtId: Primitives.requiredString()
	},
	resolve: async (rootValue, { districtId }, { req }) => {
		try {
      const foundDistrict = await DistrictModel.findOne({ _id: districtId, deletedAt: null});
      
      if (!foundDistrict) {
        return Promise.reject(new Error(req.t('notFoundDistrict')));
      }
      return foundDistrict;
		} catch (e) {
			return Promise.reject(e);
		}
	}
};