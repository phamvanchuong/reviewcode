import { GraphQLObjectType, GraphQLString as String, } from 'graphql';
import { getListDistrictsOfProvince, getDistrictDetails } from './district';
import { getListProvinces, getProvinceDetails } from './province';
import { getListWardsOfDistrict, getWardDetails } from './ward';
import { getListStreetsOfWard, getStreetDetails } from './street';
export default new GraphQLObjectType({
	name: 'Queries',
	fields: () => ({
		getListDistrictsOfProvince,
		getDistrictDetails,
		getListProvinces,
		getProvinceDetails,
		getListWardsOfDistrict,
		getWardDetails,
		getListStreetsOfWard,
		getStreetDetails
	}),
});

const greeting = {
	type: String,
	description: 'A warm welcome message from GraphQL, usually used to Test if the system working..',
	resolve: (rootValue, params, context) => {
		const { req } = context;
		return req.t('greetingQuery', { service: process.env.SERVICE_NAME });
	},
};
