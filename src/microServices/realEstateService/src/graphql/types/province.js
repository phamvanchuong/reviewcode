import { GraphQLObjectType, GraphQLString } from 'graphql';
import { Resolvers } from '../../../../../utils';
import { listDistrictsResolver } from '../resolvers';
import { DistrictType } from './district';

export const ProvinceType = new GraphQLObjectType({
	name: 'Province',
	description: 'Province Type',
	fields: () => ({
		id: Resolvers.id(),
		name: Resolvers.string('Name of province. Ex: Hà Nội'),
		shortname: Resolvers.string('short name. Ex: HN - Hà Nội'),
		districts: Resolvers.listOfType(DistrictType, 'List of labels', listDistrictsResolver),
		createdAt: Resolvers.datetime(),
		updatedAt: Resolvers.datetime(),
	})
});

export const ListProvincesType = new GraphQLObjectType({
	name: 'ListProvinces',
	description: 'Get List Provinces Type',
	fields: () => ({
		total: Resolvers.int(),
		provinces: Resolvers.listOfType(ProvinceType),
		limit: Resolvers.int(),
		page: Resolvers.int(),
		nextPage: Resolvers.int(),
		prevPage: Resolvers.int(),
		totalPages: Resolvers.int(),
		pagingCounter: Resolvers.int(),
		hasNextPage: Resolvers.boolean(),
		hasPrevPage: Resolvers.boolean()
	})
});