import { GraphQLObjectType, GraphQLString } from 'graphql';
import { Resolvers } from '../../../../../utils';
import { provinceResolver, getWardsOfDistrictResolver } from '../resolvers';
import { ProvinceType } from '../types';

export const DistrictType = new GraphQLObjectType({
	name: 'District',
	description: 'District Type',
	fields: () => ({
		id: Resolvers.id(),
		name: Resolvers.string('Name of district. Ex: Tân Bình'),
		shortname: Resolvers.string('short name. Ex: Tân Bình -> TBI'),
		province: Resolvers.ofType(ProvinceType, 'Get province', provinceResolver),
		wards: Resolvers.listOfType(ProvinceType, 'List of wards', getWardsOfDistrictResolver),
		createdAt: Resolvers.datetime(),
		updatedAt: Resolvers.datetime(),
	})
});

export const ListDistrictsType = new GraphQLObjectType({
	name: 'ListDistricts',
	description: 'Get list districts type',
	fields: () => ({
		total: Resolvers.int(),
		districts: Resolvers.listOfType(DistrictType),
		limit: Resolvers.int(),
		page: Resolvers.int(),
		nextPage: Resolvers.int(),
		prevPage: Resolvers.int(),
		totalPages: Resolvers.int(),
		pagingCounter: Resolvers.int(),
		hasNextPage: Resolvers.boolean(),
		hasPrevPage: Resolvers.boolean()
	})
});