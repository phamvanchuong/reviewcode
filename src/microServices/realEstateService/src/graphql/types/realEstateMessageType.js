import { GraphQLObjectType } from 'graphql';
import { Resolvers } from '../../../../../utils';

export const RealEstateMessageType = new GraphQLObjectType({
	name: 'RealEstateMessage',
	description: 'Response message',
	fields: () => ({
		message: Resolvers.string()
	})
});