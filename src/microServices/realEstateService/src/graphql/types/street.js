import { GraphQLObjectType, GraphQLString } from 'graphql';
import { Resolvers } from '../../../../../utils';
import { DistrictType, ProvinceType, WardType } from '.';
import { streetDistrictResolver, streetProvinceResolver, streetWardResolver } from '../resolvers';

export const StreetType = new GraphQLObjectType({
	name: 'Street',
	description: 'Street Type',
	fields: () => ({
		id: Resolvers.id(),
		name: Resolvers.string('Name of street. Ex: Nguyễn Trãi'),
		shortname: Resolvers.string('short name. Ex: 10'),
		district: Resolvers.ofType(DistrictType, 'District', streetDistrictResolver),
		province: Resolvers.ofType(ProvinceType, 'Province', streetProvinceResolver),
		ward: Resolvers.ofType(WardType, 'Province', streetWardResolver),
		isShoppingArea: Resolvers.boolean(),
    isEntertainmentArea: Resolvers.boolean(),
    isResidentialArea: Resolvers.boolean(),
    isAdministrativeArea: Resolvers.boolean(),
    isFoodnDrinkArea: Resolvers.boolean(),
    isNearSchool: Resolvers.boolean(),
		createdAt: Resolvers.datetime(),
		updatedAt: Resolvers.datetime(),
	})
});

export const ListStreetsType = new GraphQLObjectType({
	name: 'ListStreets',
	description: 'Get list streets type',
	fields: () => ({
		total: Resolvers.int(),
		streets: Resolvers.listOfType(StreetType),
		limit: Resolvers.int(),
		page: Resolvers.int(),
		nextPage: Resolvers.int(),
		prevPage: Resolvers.int(),
		totalPages: Resolvers.int(),
		pagingCounter: Resolvers.int(),
		hasNextPage: Resolvers.boolean(),
		hasPrevPage: Resolvers.boolean()
	})
});