import { GraphQLObjectType, GraphQLString } from 'graphql';
import { Resolvers } from '../../../../../utils';
import { DistrictType, StreetType } from '.';
import { wardResolver, streetResolver } from '../resolvers';

export const WardType = new GraphQLObjectType({
	name: 'Ward',
	description: 'Ward Type',
	fields: () => ({
		id: Resolvers.id(),
		name: Resolvers.string('Name of ward. Ex: Phường 10'),
		shortname: Resolvers.string('short name. Ex: 10'),
		district: Resolvers.ofType(DistrictType, 'district info', wardResolver),
		streets: Resolvers.listOfType(StreetType, 'district info', streetResolver),
		createdAt: Resolvers.datetime(),
		updatedAt: Resolvers.datetime(),
	})
});

export const ListWardsType = new GraphQLObjectType({
	name: 'ListWards',
	description: 'Get list wards type',
	fields: () => ({
		total: Resolvers.int(),
		wards: Resolvers.listOfType(WardType),
		limit: Resolvers.int(),
		page: Resolvers.int(),
		nextPage: Resolvers.int(),
		prevPage: Resolvers.int(),
		totalPages: Resolvers.int(),
		pagingCounter: Resolvers.int(),
		hasNextPage: Resolvers.boolean(),
		hasPrevPage: Resolvers.boolean()
	})
});