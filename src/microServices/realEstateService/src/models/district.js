import mongoose, { Schema } from 'mongoose';
import mongoosePaginate from 'mongoose-paginate-v2';

const districtSchema = new Schema({
	name: String,
  shortname: String,
  province: { type: Schema.Types.ObjectId, ref: 'Province' },
  wards: [{ type: Schema.Types.ObjectId, ref: 'Ward' }],
	deletedAt: { type: Date, default: null },
}, { timestamps: true });

districtSchema.index({name: 'text'});
districtSchema.plugin(mongoosePaginate);
const DistrictModel = mongoose.model('District', districtSchema);

export default DistrictModel;
