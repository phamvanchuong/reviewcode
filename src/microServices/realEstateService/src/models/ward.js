import mongoose, { Schema } from 'mongoose';
import mongoosePaginate from 'mongoose-paginate-v2';

const wardSchema = new Schema({
	name: String,
  shortname: String,
  district: { type: Schema.Types.ObjectId, ref: 'Ward' },
  streets: [{ type: Schema.Types.ObjectId, ref: 'Street' }],
	deletedAt: { type: Date, default: null },
}, { timestamps: true });

wardSchema.index({name: 'text'});
wardSchema.plugin(mongoosePaginate);
const WardModel = mongoose.model('Ward', wardSchema);

export default WardModel;
