import mongoose, { Schema } from 'mongoose';
import mongoosePaginate from 'mongoose-paginate-v2';

const realEstateSchema = new Schema({
  province: { type: Schema.Types.ObjectId, ref: 'Province', description: 'Tỉnh/TP' },
  district: { type: Schema.Types.ObjectId, ref: 'District', description: 'Phường/Xã' },
  fullAddress: String,
  isForSell: Boolean, // Bán
  isForRent: Boolean, // Cho thuê
  ward: { type: Schema.Types.ObjectId, ref: 'Ward', description: 'Phường/Xã' },
  street: { type: Schema.Types.ObjectId, ref: 'Street', description: 'Đường' },
  houseNumber: String,
  hasAlley: { type: Boolean, default: false }, //Mặt tiền -> false, Hẻm -> true
  alleyTurnCount: { type: Number, required: () => {
    return this.hasAlley;
  }}, //số lần quẹo hẻm
  timeForRent: Number, // Thời gian cho thuê
  frontHomeAlleyWidth: Number, //Độ rộng hẻm trước nhà
  isNew: { type: Boolean, default: false }, // Xây mới?
  width: Number, // Chiều rộng
  length: Number, // Chiều dài
  rear: Number, // Mặt hậu
  area: Number, // Diện tích
  price: Number, // Giá
  priceUnit: String, // Đơn vị tính
  direction: String, // Hướng
  postTitle: String, // Tiêu đề tin đăng
  content: String, // Nội dung tin đăng
  privateNote: String, // Ghi chú cá nhân
  legal: String, // Pháp lý
  legalImageUrls: [], // Hình ảnh giấy tờ pháp lý
  contactName: String, // Họ tên người liên lạc
  contactPhone1: String, // SDT1
  contactPhone2: String, // SDT2
  contactEmail: String, // Email
  imageUrls: [], // Hình ảnh
  videoUrls: [], // Video
  postType: String, // Loại tin đăng (thường/VIP 1/VIP đặc biệt)
  startShowTime: Date, // Thời gain bắt đầu đăng
  endShowTime: Date, // Thời gian dừng hiển thị
	deletedAt: { type: Date, default: null },
}, { timestamps: true, discriminatorKey: 'transactionType' });

realEstateSchema.index({name: 'text', shortname: 'text'});
realEstateSchema.plugin(mongoosePaginate);
const RealEstateModel = mongoose.model('RealEstate', realEstateSchema);




export default RealEstateModel;
