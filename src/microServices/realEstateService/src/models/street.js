import mongoose, { Schema } from 'mongoose';
import mongoosePaginate from 'mongoose-paginate-v2';

const streetSchema = new Schema({
	name: String,
  shortname: String,
  isShoppingArea: { type: Boolean, default: false }, //Khu mua sắm
  isEntertainmentArea: { type: Boolean, default: false }, //Khu vui chơi
  isResidentialArea: { type: Boolean, default: false },  //Khu dân cư
  isAdministrativeArea: { type: Boolean, default: false },  //Khu hành chính
  isFoodnDrinkArea: { type: Boolean, default: false },  //Khu ăn uống
  isNearSchool: { type: Boolean, default: false },  //Khu gần trường học
  ward: { type: Schema.Types.ObjectId, ref: 'Ward' },
  district: { type: Schema.Types.ObjectId, ref: 'District' },
  province: { type: Schema.Types.ObjectId, ref: 'Province' },
	deletedAt: { type: Date, default: null },
}, { timestamps: true });

streetSchema.index({name: 'text', shortname: 'text'});
streetSchema.plugin(mongoosePaginate);
const StreetModel = mongoose.model('Street', streetSchema);

export default StreetModel;
