import mongoose, { Schema } from 'mongoose';
import mongoosePaginate from 'mongoose-paginate-v2';

const provinceSchema = new Schema({
	name: String,
  shortname: String,
  districts: [{ type: Schema.Types.ObjectId, ref: 'District' }],
	deletedAt: { type: Date, default: null },
}, { timestamps: true });

provinceSchema.index({name: 'text', shortname: 'text'});
provinceSchema.plugin(mongoosePaginate);
const ProvinceModel = mongoose.model('Province', provinceSchema);

export default ProvinceModel;
