import express from 'express';
import { ApolloServer } from 'apollo-server-express';
import morgan from 'morgan';
import { graphQLSchema } from './src/graphql';
import cors from 'cors';
import bodyParser from 'body-parser';
import {
	i18next,
	i18nextMiddleware,
	basicAuthenticationMiddleware,
	otherServicesAuthenticationMiddleware
} from '../../utils';
import '../../utils/mongoose';

const graphQlPath = `/${process.env.SERVICE_NAME}/graphql`;

const app = express();

app.use(morgan('dev'));

app.use(
	graphQlPath,
	cors(),
	bodyParser.json(),
	basicAuthenticationMiddleware,
	otherServicesAuthenticationMiddleware,
	i18nextMiddleware.handle(i18next),
);

const server = new ApolloServer({
	schema: graphQLSchema,
	introspection: true,
	context: ({ req }) => {
		return { req };
	}
});

server.applyMiddleware({ app, path: graphQlPath });

app.listen(process.env.PORT || 1338, () => {
	console.log(`Server listen on port ${process.env.PORT || 1337}`);
});
