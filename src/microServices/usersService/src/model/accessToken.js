import mongoose, { Schema } from 'mongoose';

const accessTokenSchema = new Schema({
	token: String,
	owner: String,
	isActive: { type: Boolean, default: true }
}, { timestamps: true });

const AccessTokenModel = mongoose.model('AccessToken', accessTokenSchema);

export default AccessTokenModel;
