import mongoose, { Schema } from 'mongoose';

const permissionSchema = new Schema({
	name: String,
	description: String,
	isActive: { type: Boolean, default: true }
}, { timestamps: true });

const PermissionModel = mongoose.model('Permission', permissionSchema);

export default PermissionModel;
