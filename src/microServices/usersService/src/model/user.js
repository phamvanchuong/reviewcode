import mongoose, { Schema } from 'mongoose';
import bcrypt from 'bcryptjs';
import RoleModel from './role';

const userSchema = new Schema({
	type: {
    type: String,
    required: true,
    enum: ['HomeOwner', 'Broker', 'Company', 'User']
  },
	email: { type: String, required: true },
	fullName: String,
	userId: String,
	province: { type: String, required: true },
	isPremiumAccount: { type: Boolean, default: false },
	upgradePremiumDate: Date,
	phone: { type: String, required: true },
	secondPhone: String,
	password: String,
	isVerifiedPhone:  { type: Boolean, default: false },
	isVerifiedEmail: { type: Boolean, default: false },
	avatar: String,
	role: { type: Schema.Types.ObjectId, ref: 'Role' },
	isActive: { type: Boolean, default: true },
	activateToken: String,
	activateTokenExpires: Date,
	passwordResetToken: String,
	passwordResetTokenExpiresAt: Date,
	signupType: { type: String, required: true },
	deviceToken: String,
	isLocked: { type: Boolean, default: false },
	refMembers: [{ type: Schema.Types.ObjectId, ref: 'User' }],
	deletedAt: { type: Date, default: null },
	skype: String,
	zalo: String,
	facebook: String,
	viber: String,
	bankAccountNumber: String,
	bankName: String,
	sharePersonalInfo: { type: Boolean, default: false },
	shareOwnedPosts: { type: Boolean, default: false }
}, { timestamps: true, discriminatorKey: 'type' });

userSchema.statics.emailExist = function (email) {
	return this.findOne({ email })
};

userSchema.methods.comparePassword = function (password) {
	return bcrypt.compareSync(password, this.password)
};

userSchema.statics.phoneExist = function (phone) {
	return this.findOne({ phone })
};

userSchema.methods.getRolesWithPermissions = async function(){
	return RoleModel.find({ _id: this.role }).populate({ path: 'permissions', select: 'name' });
};

const UserModel = mongoose.model('User', userSchema);


const homeOwnerSchema = new Schema({
	fullName: { type: String, required: true },
	birthDate: { type: Date, required: true },
	gender: { type: String, required: true },
	idNumber: { type: String, required: true }
}, { timestamps: true, discriminatorKey: 'type' });

const HomeOwnerModel = UserModel.discriminator('HomeOwner', homeOwnerSchema);

const brokerSchema = new Schema({
	fullName: { type: String, required: true },
	birthDate: { type: Date, required: true },
	gender: { type: String, required: true },
	idNumber: { type: String, required: true },
	company: { type: String, required: true } //Thuộc về cty nào (id)
}, { timestamps: true, discriminatorKey: 'type' });

const BrokerModel = UserModel.discriminator('Broker', brokerSchema);

const companySchema = new Schema({
	companyName: { type: String, required: true },
	taxId: { type: String, required: true },
	companyType: { type: String, required: true },
	establishedDate: { type: Date, required: true },
	isOfficial: { type: Boolean, default: false }, //true -> Account chính thức / false -> User tự tạo
	description: String // Mô tả doanh nghiệp

}, { timestamps: true, discriminatorKey: 'type' });

const CompanyModel = UserModel.discriminator('Company', companySchema);

export { UserModel, HomeOwnerModel, BrokerModel, CompanyModel };
