import mongoose, { Schema } from 'mongoose';

const verificationCodeSchema = new Schema({
	phone: String,
    expiresIn: Date,
    verifyCode: String,
    isVerified: { type: Boolean, default: false},
    duplicatedCount: Number,
}, { timestamps: true });

const VerificationCodeModel = mongoose.model('VerificationCode', verificationCodeSchema);

export default VerificationCodeModel;
