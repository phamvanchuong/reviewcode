import mongoose, { Schema } from 'mongoose';

const roleSchema = new Schema({
	name: String,
	description: String,
    isActive: { type: Boolean, default: true },
    permissions: [{ type: Schema.Types.ObjectId, ref: 'Permission' }]
}, { timestamps: true });

const RoleModel = mongoose.model('Role', roleSchema);

export default RoleModel;
