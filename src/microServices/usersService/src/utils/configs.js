export const configs = {
	loginBy: ['email', 'phone', 'username'],
	shouldVerify: ['email', 'phone'],
};
