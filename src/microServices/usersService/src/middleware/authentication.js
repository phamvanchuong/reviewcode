import jwt from 'jsonwebtoken';
import { UserModel } from '../model/user';
import AccessTokenModel from '../model/accessToken';

export async function authenticationMiddleware(req, res, next) {
	try {
		const { headers: { authorization } } = req;
		if (!authorization) {
			return next()
		}
		const accessToken = authorization.split(' ')[1];

		const decoded = jwt.verify(accessToken, process.env.JWT_SECRET);
		if (!decoded) {
			return next()
		}

		const jwtToken = await AccessTokenModel.findOne({ token: accessToken, isActive: true });
		if(!jwtToken) {
			return next();
		}
		const user = await UserModel.findById(decoded.userId);
		// 	{ shouldVerify = [] } = configs;
		// if (shouldVerify.indexOf('email') && !user.verifiedEmail) {
		// 	return Promise.reject(new Error('Email not verified'));
		// }
		// if (shouldVerify.indexOf('phone') && !user.verifiedPhone) {
		// 	return Promise.reject(new Error('Phone not verified'));
		// }
		if (!user) {
			return next()
		}
		Object.assign(req, {
			user,
			accessToken
		});
		return next()
	} catch (e) {
		return next()
	}
}
