import { GraphQLObjectType, GraphQLString as String, } from 'graphql';
import { user, getUserInfo, getListOfUsers, checkPermission, init } from './user';
import { isAdmin, isAuthenticated, isBasicAuthenticated, isAuthenticatedToViewListOfUser } from '../middleware';
import { middlewareResolver } from '../../../../../utils';
import { viewRole } from './role';

export default new GraphQLObjectType({
	name: 'Queries',
	fields: () => ({
		user: middlewareResolver(user, [isAuthenticated]),
		getUserInfo: middlewareResolver(getUserInfo, [isBasicAuthenticated]),
		getListOfUsers: middlewareResolver(getListOfUsers, [isAuthenticatedToViewListOfUser]),
		viewRole: middlewareResolver(viewRole, [isAdmin]),
		checkPermission: middlewareResolver(checkPermission, [isBasicAuthenticated]),
		init
	}),
});

