import { UserType, UserConditionInputType, HasPermissionType, MessageType } from '../types';
import { UserModel } from '../../model/user';
import { Primitives, initialize } from '../../../../../utils';
import { GraphQLList } from 'graphql';
import RoleModel from '../../model/role';


export const user = {
	type: UserType,
	description: 'Get profile of a User',
	resolve: async (root, args, context) => {
		try {
			const { user: authUser } = context.req,
				user = await UserModel.findById(authUser._id);
			if (!user) {
				return Promise.reject(new Error('User not found'));
			}
			return user;
		} catch (error) {
			return Promise.reject(error);
		}
	},
};

export const getUserInfo = {
	type: UserType,
	description: 'Get profile of a User',
	args: {
		id: Primitives.requiredString()
	},
	resolve: async (root, { id }, context) => {
		try {
			const user = await UserModel.findOne({ _id: id, deletedAt: null });
			if (!user) {
				return Promise.reject(new Error(context.req.t('userNotFound')));
			}
			return user;
		} catch (error) {
			return Promise.reject(error);
		}
	},
};

export const getListOfUsers = {
	type: GraphQLList(UserType),
	description: 'Get list of user base on conditions',
	args: {
		conditions: Primitives.ofType(UserConditionInputType)
	},
	resolve: async (root, { conditions }) => {
		try {
			const { id, email, roleId: role } = conditions,
				conditionsObj = { id, email, role },
				payload = {};
			Object.entries(conditionsObj).forEach(([key, value]) => {
				if (value) {
					if (key === 'id') {
						payload[`_${key}`] = {
						 $in: value
						}
					} else payload[key] = value;
				}
			});
			return await UserModel.find(payload);
		} catch (error) {
			return Promise.reject(error);
		}
	},
};

export const checkPermission = {
	type: HasPermissionType,
	description: 'Check if user has provided permission',
	args: {
		userId: Primitives.requiredString(),
		action: Primitives.requiredString(),
	},
	resolve: async (root, { userId, action }, { req }) => {
		try {
			const foundUser = await UserModel.findOne({ _id: userId, deletedAt: null, isActive: true });
			if (!foundUser)
				return Promise.reject(new Error(req.t('userNotFound')));

			if (!foundUser.role) {
				return Promise.reject(new Error(req.t('roleNotFound')));
			}

			const userRole = await RoleModel.findOne({ _id: foundUser.role, deletedAt: null }).populate({ path: 'permissions', select: 'name' });

			if (!userRole)
				return Promise.reject(new Error(req.t('roleNotFound')));

			if (!userRole.permissions.length)
				return Promise.reject(new Error(req.t('permissionNotSet')));
			return { has: userRole.permissions.some(e => e.name === action) };

		} catch (error) {
			return Promise.reject(error);
		}
	},
};

export const init = {
	type: MessageType,
	description: 'Create first admin user and permissions',
	args: {
		pass: Primitives.requiredString('Enter password to run')
	},
	resolve: async (root, { pass }) => {
		try {
			if (!pass) {
				return Promise.reject(new Error('Password required. Please contact supporter for assistant.'))
			}
			// TODO move password to env file
			if (pass !== 'OcrSetUp@!') {
				return Promise.reject(new Error('Wrong password. Please check and try again.'))
			}
			const foundUser = await UserModel.findOne({ email: 'admin@example.com', deletedAt: null, isActive: true });
			if (!foundUser)
				return initialize();
			else
				return { message: 'Already run.'};

		} catch (error) {
			return Promise.reject(error);
		}
	},
};
