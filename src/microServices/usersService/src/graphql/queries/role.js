import { Primitives } from '../../../../../utils';
import { RoleType } from '../types/user'
import { roleResolversByUserRoleOrRoleId } from '../resolvers';

export const viewRole = {
	type: RoleType,
	description: 'Get details of an existing role',
	args: {
		id: Primitives.requiredString()
	},
	resolve: roleResolversByUserRoleOrRoleId,
};
