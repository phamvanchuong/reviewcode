import bcrypt from 'bcryptjs';
import moment from 'moment';
import { Primitives, phoneValidator } from '../../../../../utils';
import {
	MessageType,
	UpdateUserInputType,
	UserType,
	CreateUserInputType
} from '../types';
import { UserModel } from '../../model/user';
import AccessTokenModel from '../../model/accessToken';
import { random6digits, uidLight, sendSMS, sendEmail } from '../../../../../utils';
import RoleModel from '../../model/role';

export const createPassword = {
	type: MessageType,
	description: 'Create password',
	args: {
		password: Primitives.requiredString()
	},
	resolve: async (rootValue, { password }, { req }) => {
		try {
			const { user } = req;
			if (!user) {
				return Promise.reject(new Error(req.t('userNotFound')));
			} else {

				if (user.isPasswordCreated) {
					return { message: req.t('passwordAlreadyCreated') };
				}

				const updateUser = await UserModel.findById(user._id);

				if (!updateUser) {
					return Promise.reject(new Error(req.t('userNotFound')))
				}
				updateUser.password = bcrypt.hashSync(password, 10);
				await updateUser.save();

				return { message: req.t('createPasswordSuccess') }
			}

		} catch (e) {
			return Promise.reject(e);
		}
	}
};

export const forgotPasswordPhone = {
	type: MessageType,
	description: 'Forgot password phone',
	args: {
		phone: Primitives.requiredString()
	},
	resolve: async (rootValue, { phone }, { req }) => {
		try {
			if (!phoneValidator(phone)) {
				return Promise.reject(new Error(req.t('invalidPhoneNumber')));
			}

			const user = await UserModel.findOne({ phone, isActive: true, deletedAt: null });

			if (!user) {
				return Promise.reject(new Error(req.t('userNotFound')));
			}
			const OTP = random6digits(),
				message = `(#) ${process.env.APP_NAME} \n${OTP} is your reset password code. This code is valid within 15 minutes.`;
			const { loading, error, data } = await sendSMS(phone, message);
			if (error) {
				console.log(error)
				return new Promise.reject(error);
			}
			if(!loading && data) {
				user.passwordResetToken = OTP;
				user.passwordResetTokenExpiresAt = moment().add(process.env.activateTokenExpires, 'minutes');
				user.save();

				return { message: req.t('OTPSent') }
			} else {
				return Promise.reject(data);
			}

		} catch (e) {
			console.log(e)
			return Promise.reject(e);
		}
	}
};

export const resetPassword = {
	type: MessageType,
	description: 'Reset password',
	args: {
		phone: Primitives.requiredString(),
		newPassword: Primitives.requiredString(),
		secretCode: Primitives.requiredString()
	},
	resolve: async (rootValue, { phone, newPassword, secretCode }, { req }) => {
		try {

			const user = await UserModel.findOne({
				phone: phone,
				isActive: true
			});
			if (!user) {
				return Promise.reject(new Error(req.t('userNotFound')));
			}

			if (user.passwordResetToken === secretCode) {

				if (moment() >= user.passwordResetTokenExpiresAt) {
					return Promise.reject(new Error(req.t('expiredSecretCode')));
				}
				user.password = bcrypt.hashSync(newPassword, 10);
				user.passwordResetToken = null;
				await user.save();
				await AccessTokenModel.findOneAndDelete({ owner: user.id });

				return { message: req.t('resetPasswordSuccess') };
			} else {
				return Promise.reject(new Error(req.t('invalidSecret')));
			}

		} catch (e) {
			return Promise.reject(e);
		}
	}
};

export const updateUser = {
	type: MessageType,
	description: 'Update user profile',
	args: {
		firstName: Primitives.string(),
		lastName: Primitives.string(),
		email: Primitives.string(),
		avatar: Primitives.string(),
		phone: Primitives.string(),
		birthDate: Primitives.string(),
		role: Primitives.string()
	},
	resolve: async (rootValue, args, { req }) => {
		try {
			const { user: oldUser } = req;
			if (!oldUser) {
				return Promise.reject(new Error(req.t('userNotFound')));
			}
			const user = await UserModel.findById(oldUser._id);
			if (!user) {
				return Promise.reject(new Error(req.t('userNotFound')));
			}
			if (args.email) {
				if (args.email === user.email) {
					return Promise.reject(new Error(req.t('emailIsUsing')));
				}
				const checkUserEmail = await UserModel.emailExist(args.email);
				if (checkUserEmail) {
					return Promise.reject(new Error(req.t('emailTaken')));
				}
			}

			if (args.phone) {
				if (!phoneValidator(args.phone)) {
					return Promise.reject(new Error(req.t('invalidPhoneNumber')));
				}
				if (args.phone === user.phone) {
					return Promise.reject(new Error(req.t('phoneIsUsing')));
				}
				const checkUserPhone = await UserModel.phoneExist(args.phone);
				if (checkUserPhone) {
					return Promise.reject(new Error(req.t('phoneExist')));
				}
			}

			if (args.birthDate)
				args.birthDate = moment(args.birthDate);

			if (args.role) {
				const role = await RoleModel.findOne({ _id: args.role, isActive: true });
				if (!role) {
					return Promise.reject(new Error(req.t('roleNotFound')));
				}
			}

			await UserModel.findByIdAndUpdate(user._id, args);

			return { message: req.t('updateProfileSuccess') };

		} catch (e) {
			return Promise.reject(e);
		}
	}
};

export const forgotPasswordEmail = {
	type: MessageType,
	description: 'Forgot password email',
	args: {
		email: Primitives.requiredString(),
		stringToken: Primitives.boolean(),
		resetLink: Primitives.string(),
		resend: Primitives.boolean()
	},
	resolve: async (rootValue, { email, stringToken, resetLink, resend }, { req }) => {
		try {

			stringToken = stringToken || false,
			resetLink = resetLink || `${process.env.BASE_URL}/reset-password`;

			const user = await UserModel.findOne({ email: email, isActive: true, deletedAt: null });
			if (!user) {
				return Promise.reject(new Error(req.t('userNotFound')));
			}

			const randomString = uidLight(32),
				passwordResetToken = stringToken ? randomString : random6digits();

			await UserModel.findByIdAndUpdate(user._id, {
				passwordResetToken: passwordResetToken,
				passwordResetTokenExpiresAt: moment().add(process.env.passwordResetTokenTTL, 'hours')
			});

			//
			if (!stringToken) {
				const variables = {
					firstName: user.firstName,
					OTP: passwordResetToken,
					appName: process.env.APP_NAME
				};

				sendEmail(email, `${process.env.APP_NAME} Reset Password`, 'email-forgot-password', JSON.stringify(variables), null);
			} else {

				const variables = {
					firstName: user.firstName,
					resetPasswordLink: resetLink + '/' + passwordResetToken,
					appName: process.env.APP_NAME
				};

				sendEmail(email, `${process.env.APP_NAME} Reset Password`, 'email-forgot-password-link', variables, null);
			}

			return { message: req.t('emailSent') }


		} catch (e) {
			return Promise.reject(e);
		}
	}
};

export const updateUserByAnotherRole = {
	type: UserType,
	description: 'Update user profile',
	args: {
		id: Primitives.requiredString(),
		updateData: Primitives.ofType(UpdateUserInputType)
	},
	resolve: async (rootValue, { id, updateData }, { req }) => {
		try {
			const { firstName, lastName, email, phone, avatar, role, birthDate, password } = updateData,
				payload = {};
			const user = await UserModel.findOne({ _id: id, isActive: true, deletedAt: null });
			if (!user) {
				return Promise.reject(new Error(req.t('userNotFound')));
			}

			if (email) {
				if (email === user.email) {
					return Promise.reject(new Error(req.t('emailIsUsing')));
				}
				const checkUserEmail = await UserModel.emailExist(email);
				if (checkUserEmail) {
					return Promise.reject(new Error(req.t('emailTaken')));
				}
			}

			if (phone) {
				if (phone === user.phone) {
					return Promise.reject(new Error(req.t('phoneIsUsing')));
				}
				const checkUserPhone = await UserModel.phoneExist(phone);
				if (checkUserPhone) {
					return Promise.reject(new Error(req.t('phoneExist')));
				}
			}

			if (birthDate) payload.birthDate = moment(birthDate).format('YYYYMMDD');

			if (role) {
				const role = await RoleModel.findOne({ _id: role, isActive: true });
				if (!role) {
					return Promise.reject(new Error(req.t('roleNotFound')));
				}
			}

			if (password) {
				payload.password = bcrypt.hashSync(password, 10);
			}
			let updateObj = { firstName, lastName, phone, avatar, email, role, birthDate, password };
			Object.entries(updateObj).forEach(([key, value]) => {
				if (value && !payload[key]) {
					payload[key] = value
				}
			});

			return await UserModel.findByIdAndUpdate(user._id, payload);

		} catch (e) {
			return Promise.reject(e);
		}
	}
};

export const removeUserByAnotherRole = {
	type: UserType,
	description: 'Remove user',
	args: {
		id: Primitives.requiredString()
	},
	resolve: async (rootValue, { id }, { req }) => {
		try {

			const user = await UserModel.findOne({ _id: id, isActive: true, deletedAt: null });
			if (!user) {
				return Promise.reject(new Error(req.t('userNotFound')));
			}

			user.isActive = false;
			user.deletedAt = moment();
			await user.save();
			return user;

		} catch (e) {
			return Promise.reject(e);
		}
	}
};

export const createUserByAnotherRole = {
	type: UserType,
	description: 'Create a new user',
	args: {
		data: Primitives.ofType(CreateUserInputType)
	},
	resolve: async (rootValue, { data }, { req }) => {
		try {

			const { firstName, lastName, email, phone, avatar, role, birthDate, password } = data,
				userObj = { firstName, lastName, phone, avatar, email, role, birthDate, password },
				payload = {};

			if (email) {
				const checkUserEmail = await UserModel.emailExist(email);
				if (checkUserEmail) {
					return Promise.reject(new Error(req.t('emailTaken')));
				}
			}

			if (phone) {
				const checkUserPhone = await UserModel.phoneExist(phone);
				if (checkUserPhone) {
					return Promise.reject(new Error(req.t('phoneExist')));
				}
			}

			if (birthDate) payload.birthDate = moment(birthDate).format('YYYYMMDD');

			if (role) {
				const foundRole = await RoleModel.findOne({ _id: role, isActive: true });
				if (!foundRole) {
					return Promise.reject(new Error(req.t('roleNotFound')));
				}
			}

			Object.entries(userObj).forEach(([key, value]) => {
				if (value && !payload[key]) {
					payload[key] = value
				}
			});
			const newUser = await new UserModel(payload).save();
			return newUser;
		} catch (e) {
			return Promise.reject(e);
		}
	}
};

export const resetPasswordEmail = {
	type: MessageType,
	description: 'Reset password via email',
	args: {
		email: Primitives.requiredString(),
		newPassword: Primitives.requiredString(),
		secretCode: Primitives.requiredString()
	},
	resolve: async (rootValue, { email, newPassword, secretCode }, { req }) => {
		try {

			const user = await UserModel.findOne({
				email,
				isActive: true,
				deletedAt: null
			});
			if (!user) {
				return Promise.reject(new Error(req.t('userNotFound')));
			}

			if (user.passwordResetToken === secretCode) {

				if (moment() >= user.passwordResetTokenExpiresAt) {
					return Promise.reject(new Error(req.t('expiredSecretCode')));
				}
				user.password = bcrypt.hashSync(newPassword, 10);
				user.passwordResetToken = null;
				await user.save();
				await AccessTokenModel.findOneAndDelete({ owner: user.id });

				return { message: req.t('resetPasswordSuccess') };
			} else {
				return Promise.reject(new Error(req.t('invalidSecret')));
			}

		} catch (e) {
			return Promise.reject(e);
		}
	}
};