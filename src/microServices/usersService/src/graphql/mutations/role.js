import { Primitives } from '../../../../../utils';
import { UserModel } from '../../model/user';
import { GraphQLString } from 'graphql';
import RoleModel from '../../model/role';
import PermissionModel from '../../model/permission';
import { RoleType, UpdateRoleInputType } from '../types'

export const createRole = {
	type: RoleType,
	description: 'Create a new role',
	args: {
			name: Primitives.requiredString('Name of role without special characters.'),
			description: Primitives.string(),
			permissions: Primitives.list(GraphQLString, [], 'List name of permissions')
	},
	resolve: async (rootValue, args, { req }) => {
		try {
			const { user } = req;
			if (!user) {
				return Promise.reject(new Error(req.t('userNotFound')));
			} else {
				const roleName = args.name.toLowerCase().replace(' ', '-'),
					foundRole = await RoleModel.where({ name: roleName });
				if (foundRole.length) {
					return Promise.reject(new Error(req.t('roleExist')));
				}
				const role = await new RoleModel(args).save();
				// Create related permissions
				await PermissionModel.create([{ name: `create-user-${args.name}` }, { name: `update-user-${args.name}` }, { name: `view-user-${args.name}` }, { name: `delete-user-${args.name}` }]);
				return role;
			}

		} catch (e) {
			return Promise.reject(e);
		}
	}
};

export const updateRole = {
	type: RoleType,
	description: 'Update an existing role',
	args: {
		id: Primitives.requiredString(),
		updateData: Primitives.ofType(UpdateRoleInputType)
	},
	resolve: async (rootValue, { id, updateData }, { req }) => {
		try {
			const { user } = req;
			if (!user) {
				return Promise.reject(new Error(req.t('userNotFound')));
			}
			const foundRole = await RoleModel.findByIdAndUpdate(id, updateData).populate({ path: 'permissions', select: 'name' });
			if (!foundRole) {
				return Promise.reject(new Error(req.t('roleNotFound')));
			}
			return foundRole;
		} catch (e) {
			return Promise.reject(e);
		}
	}
};

export const removeRole = {
	type: RoleType,
	description: 'Remove an existing role',
	args: {
		id: Primitives.requiredString()
	},
	resolve: async (rootValue, { id }, { req }) => {
		try {
			const { user } = req;
			if (!user) {
				return Promise.reject(new Error(req.t('userNotFound')));
			}
			const foundRole = await UserModel.find({ role: id });
			if (foundRole.length) {
				return Promise.reject(new Error(req.t('roleInUsed')));
			}
			const deleteRole = await RoleModel.findByIdAndDelete({ _id: id });
			if (!deleteRole) {
				return Promise.reject(new Error(req.t('roleNotFound')));
			}
			return deleteRole;
		} catch (e) {
			return Promise.reject(e);
		}
	}
};
