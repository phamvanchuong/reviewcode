import bcrypt from 'bcryptjs';
import jwt from 'jsonwebtoken';
import { UserInputError } from 'apollo-server-express';
import moment from 'moment';
import { Primitives, userType,
	phoneValidator,
	sendSMS,
	province,
	random6digits,
	redisClient,
	addRegionPhoneCode } from '../../../../../utils';
import {
	AccessTokenType,
	MessageType,
	SocialLoginResponseType,
	UserType,
	SignUpDataInputType,
} from '../types';
import { UserModel, HomeOwnerModel, BrokerModel, CompanyModel } from '../../model/user';
import AccessTokenModel from '../../model/accessToken';
import VerificationCodeModel from '../../model/phoneVerifyCode';
import * as admin from "firebase-admin";

export const signIn = {
	type: AccessTokenType,
	description: 'Sign in an user',
	args: {
		emailOrPhone: Primitives.requiredString(),
		password: Primitives.requiredString()
	},
	resolve: async (rootValue, { emailOrPhone, password }, { req }) => {
		try {
			const user = await UserModel.findOne({ $or: [{ email: emailOrPhone }, { phone: emailOrPhone }]});
			if (!user) {
				return Promise.reject(new Error(req.t('wrongCredential')));
			}
			const comparePassword = await user.comparePassword(password);
			if (!comparePassword) {
				return Promise.reject(new Error(req.t('wrongCredential')));
			}
			const accessToken = jwt.sign(
				{ userId: user._id },
				process.env.JWT_SECRET,
				{ expiresIn: process.env.JWT_EXPIRATION }
			);

			await AccessTokenModel.findOneAndUpdate(
				{ owner: user._id },
				{ token: accessToken, isActive: true },
				{ upsert: true }
			);

			return {
				accessToken
			}
		} catch (e) {
			return Promise.reject(e);
		}
	}
};

export const signUp = {
	type: AccessTokenType,
	description: 'Sign up an user',
	args: {
		type: Primitives.requiredString(),
		data: Primitives.ofType(SignUpDataInputType, {}, 'Sign up data object')
	},
	resolve: async (rootValue, { type, inputData }, { req }) => {
		try {
			const { email, phone, password, province: inputProvince } = inputData,
				data = {
					...inputData,
					password: bcrypt.hashSync(password, 10),
					type,
				}
			const usedEmail = await UserModel.emailExist(email),
			phoneVerification = await VerificationCodeModel.findOne({ phone, isVerified: true });
			if (usedEmail) {
				return Promise.reject(new UserInputError(req.t('emailTaken')));
			}

			if (!phoneVerification) {
				return Promise.reject(new UserInputError(req.t('phoneNotVerified')));
			}
			if (inputProvince) {
				const foundProvince = province.filter( c => c.id === inputProvince);
				if (!foundProvince.length) {
					return Promise.reject(new UserInputError(req.t('notFoundProvince')));
				}
			}
			data.type = type;
			// TODO update role
			let newUser;
			switch (type) {
				case userType.homeOwner.en:
					newUser = await new HomeOwnerModel(data).save();
					break;
				case userType.broker.en:
					newUser = await new BrokerModel(data).save();
					break;
				case userType.company.en:
					newUser = await new CompanyModel(data).save();
					break;
				default:
					newUser = await new UserModel(data).save();
					break;
			}
			const accessToken = jwt.sign(
				{ userId: newUser._id },
				process.env.JWT_SECRET,
				{ expiresIn: process.env.JWT_EXPIRATION }
			);
			await new AccessTokenModel({
				token: accessToken,
				owner: newUser.id
			}).save();
			return {
				accessToken
			}
		} catch (e) {
			return Promise.reject(e);
		}
	}
};

export const logOut = {
	type: MessageType,
	description: 'Sign user out',
	args: {
	},
	resolve: async (rootValue, args, { req }) => {
		try {
			const { user } = req;
			const accessToken = await AccessTokenModel.find({ token: user.accessToken, owner: user.userId, isActive: true });
			if (!accessToken) {
				return Promise.reject(new Error(req.t('notLoggedIn')));
			}
			await AccessTokenModel.findOneAndUpdate({ id: accessToken.id, isActive: false });
			await UserModel.updateOne({ id: user.userId, deviceToken: '' });

			return { message: req.t('logOutSuccess') };
		} catch (e) {
			return Promise.reject(e);
		}
	}
};

export const socialLogin = {
	type: SocialLoginResponseType,
	description: 'Sign in with Google account',
	args: {
		idToken: Primitives.requiredString()
	},
	resolve: async (rootValue, { idToken }) => {
		try {
			const decodedToken = await admin.auth().verifyIdToken(idToken);
			const user = await UserModel.findOne({ email: decodedToken.email });
			if (user) {
				const accessToken = jwt.sign(
					{ userId: user._id },
					process.env.JWT_SECRET,
					{ expiresIn: process.env.JWT_EXPIRATION }
				);

				await AccessTokenModel.findOneAndUpdate(
					{ owner: user._id },
					{ token: accessToken, isActive: true },
					{ upsert: true }
				);

				return { accessToken };

			} else {
				const  name = decodedToken.name.split(' '),
					firstName = name[0],
					lastName = decodedToken.name.substring(firstName.length, decodedToken.name.length).trim();
				return {
					firstName: firstName,
					lastName: lastName,
					email: decodedToken.email
				};
			}
		} catch (e) {
			return Promise.reject(e);
		}
	}
};

export const verifyToken = {
	type: UserType,
	description: 'Verify token',
	args: {
		token: Primitives.requiredString()
	},
	resolve: async (rootValue, { token }, { req }) => {
		try {
			const decoded = jwt.verify(token, process.env.JWT_SECRET);
			if (!decoded) {
				return Promise.reject(Error(req.t('invalidToken')));
			}
			const foundToken = await AccessTokenModel.findOne({ token: token, isActive: true });
			if (!foundToken) {
				return Promise.reject(Error(req.t('invalidToken')));
			} else {
				const user = await UserModel.findById(foundToken.owner);

				if (!user) {
					return Promise.reject(Error(req.t('userNotFound')));
				}
				await redisClient.set(token, JSON.stringify(user));
				return user;
			}

		} catch (e) {
			return Promise.reject(e);
		}
	}
};

export const verifyPhone = {
	type: MessageType,
	description: 'Verify phone number when register new account',
	args: {
		phone: Primitives.requiredString(),
		OTP: Primitives.requiredString()
	},
	resolve: async (rootValue, { phone, OTP }, { req }) => {
		try {
			const foundVerification = await VerificationCodeModel.findOne({ phone, verifyCode: OTP });
			if (!foundVerification) {
				return Promise.reject(new Error(req.t('invalidSecret')));
			} else {
				if (moment() >= foundVerification.expiresIn) {
					return Promise.reject(new Error(req.t('expiredSecretCode')));
				}
			}
			foundVerification.isVerified = true;
			await foundVerification.save();
			return { message: req.t('successVerification') };
		} catch (e) {
			return Promise.reject(e);
		}
	}
};

export const sendVerifyCode = {
	type: MessageType,
	description: 'Send verify code to phone number when register new account',
	args: {
		phone: Primitives.requiredString(),
		region: Primitives.string()
	},
	resolve: async (rootValue, { phone, region = 'vn' }, { req }) => {
		try {
			if (!phoneValidator(phone)) {
				return Promise.reject(new Error(req.t('invalidPhoneNumber')));
			}
			const fullPhoneNumber = addRegionPhoneCode(region, phone);
			const OTP = random6digits(),
				message = `(#) ${process.env.APP_NAME} \n${OTP} ${req.t('contentVerifyPhone')}`;
			const { loading, error, data } = await sendSMS(fullPhoneNumber, message);
			if (error) {
				return new Promise.reject(error);
			}
			if(!loading && data) {
				await VerificationCodeModel.findOneAndUpdate({ phone }, { verifyCode: OTP, expiresIn: moment().add(process.env.activateTokenExpires, 'minutes'), isVerified: false }, { upsert: true });

				return { message: req.t('OTPSent') }
			} else {
				return { message: req.t('OTPSentFailed') }
			}
		} catch (e) {
			return Promise.reject(e);
		}
	}
};
