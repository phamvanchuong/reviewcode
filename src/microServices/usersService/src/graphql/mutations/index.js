import { GraphQLObjectType } from 'graphql';
import { GraphQLString as String } from 'graphql/type/scalars';
import { 
	isAdmin, 
	isAuthenticated, 
	isAuthenticatedToUpdateUser, 
	isAuthenticatedToRemoveUser, 
	isAuthenticatedToCreateUser 
} from '../middleware';
import { middlewareResolver } from '../../../../../utils';

import { signUp, signIn, logOut, socialLogin, verifyToken, verifyPhone, sendVerifyCode } from './auth';
import { 
	createPassword, 
	forgotPasswordPhone, 
	resetPassword, 
	resetPasswordEmail,
	updateUser, 
	forgotPasswordEmail, 
	updateUserByAnotherRole,
	removeUserByAnotherRole,
	createUserByAnotherRole
} from './user';
import { createRole, updateRole, removeRole } from './role';

export default new GraphQLObjectType({
	name: 'Mutations',
	fields: () => ({
		[`${process.env.SERVICE_NAME}Greeting`]: greeting,
		signUp,
		signIn,
		logOut: middlewareResolver(logOut, [isAuthenticated]),
		socialLogin,
		verifyToken,
		forgotPasswordPhone,
		createPassword: middlewareResolver(createPassword, [isAuthenticated]),
		resetPassword,
		resetPasswordEmail,
		verifyPhone,
		sendVerifyCode,
		updateUser: middlewareResolver(updateUser, [isAuthenticated]),
		forgotPasswordEmail,
		createRole: middlewareResolver(createRole, [isAdmin]),
		updateRole: middlewareResolver(updateRole, [isAdmin]),
		removeRole: middlewareResolver(removeRole, [isAdmin]),
		updateUserByAnotherRole: middlewareResolver(updateUserByAnotherRole, [isAuthenticated, isAuthenticatedToUpdateUser]),
		removeUserByAnotherRole: middlewareResolver(removeUserByAnotherRole, [isAuthenticated, isAuthenticatedToRemoveUser]),
		createUserByAnotherRole: middlewareResolver(createUserByAnotherRole, [isAuthenticated, isAuthenticatedToCreateUser]),
	}),
});

const greeting = {
	type: String,
	description: 'A warm welcome message from GraphQL, usually used to Test if the system working..',
	resolve: (rootValue, params, context) => {
		const { req } = context;
		return req.t('greetingMutation', { service: process.env.SERVICE_NAME });
	},
};
