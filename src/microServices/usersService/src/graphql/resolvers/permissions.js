import PermissionModel from '../../model/permission';

export async function permissionResolvers(role) {
	try {
		const { permissions: permissionIdsList } = role;
		return PermissionModel.find({
			'_id': permissionIdsList
		});
	} catch (e) {
		return Promise.reject(e);
	}
}
