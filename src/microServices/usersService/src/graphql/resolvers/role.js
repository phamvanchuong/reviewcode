import RoleModel from '../../model/role';

export async function roleResolversByUserRoleOrRoleId(user, args, { req }) {
	try {
		let foundRole;
		if (args.id) {
			foundRole = await RoleModel.findById({ _id: id });
			if (!foundRole) {
				return Promise.reject(new Error(req.t('roleNotFound')));
			}
		} else if (user.role) {
			foundRole = await RoleModel.findById({ _id: user.role });
		}
		return foundRole;
	} catch (e) {
		return Promise.reject(e);
	}
}
