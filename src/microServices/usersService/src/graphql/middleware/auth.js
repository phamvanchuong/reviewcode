import RoleModel from '../../model/role';
import { AuthenticationError, UserInputError } from 'apollo-server-express';
import { UserModel } from '../../model/user';

export const isAuthenticated = (root, args, context) => {
	if (!context.req.user) {
		return new AuthenticationError('Not authenticated')
	}
};

export const isAdmin = async (root, args, { req }) => {
	if (!req.user) {
		return new AuthenticationError('Unauthenticated');
	}
	const role = await RoleModel.findById(req.user.role);
	if (!role || role.name !== 'admin' || role.name !== 'super_admin') {
		return new AuthenticationError('You are not admin');
	}
};

export const isBasicAuthenticated = (root, args, context) => {
	if (!context.req.basicAuthenticated) {
		return new AuthenticationError('Not authenticated')
	}
};

async function isAuthenticatedToSpecificUser({ userId, roleId }, req, action = 'view') { // <~ userId: user to view / remove / update;
	const { user: authUser } = req;
	let toActionRole;
	if (userId) {
		const userToAction = await UserModel.findById(userId);
		if (!userToAction) {
			return Promise.reject(new UserInputError(req.t('userNotFound')));
		}
		toActionRole = (await RoleModel.findById(userToAction.role)?.name);
	}
	if (roleId) {
		toActionRole = (await RoleModel.findById(roleId)?.name);
	}
	if (!toActionRole) {
		return Promise.reject(new UserInputError(`Role not found`));
	}
	const { permissions } = await RoleModel.findById(authUser.role).populate({ path: 'permissions', select: 'name' }),
		canAction = permissions.some(e => e.name === `${action}-user-${toActionRole}`);
	if (!canAction) {
		return Promise.reject(new AuthenticationError(`You do not have permission to ${action} user with id: ${userId}`));
	}
}

export const isAuthenticatedToViewListOfUser = async (root, args, { req }) => {
	try {
		return isAuthenticatedToSpecificUser({ roleId: args.conditions?.roleId }, req, 'view');
	} catch (e) {
		return Promise.reject(e);
	}
};

export const isAuthenticatedToUpdateUser = async (root, args, { req }) => {
	try {
		return isAuthenticatedToSpecificUser({ userId: args.id }, req, 'update');
	} catch (e) {
		return Promise.reject(e);
	}
};

export const isAuthenticatedToRemoveUser = async (root, args, { req }) => {
	try {
		return isAuthenticatedToSpecificUser({ userId: args.id }, req, 'delete');
	} catch (e) {
		return Promise.reject(e);
	}
};

export const isAuthenticatedToCreateUser = async (root, { data }, { req }) => {
	try {
		const { role: roleId } = data;
		return isAuthenticatedToSpecificUser({ roleId }, req, 'create');
	} catch (e) {
		return Promise.reject(e);
	}
};
