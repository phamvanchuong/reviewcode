import { GraphQLInputObjectType, GraphQLUnionType, GraphQLEnumType } from 'graphql';
import { GraphQLJSON, GraphQLJSONObject } from 'graphql-type-json'
import { Primitives } from '../../../../../../utils';

const RegisterEnumType = new GraphQLEnumType({
  name: 'RegisterEnum',
  values: {
    HOME_OWNER: {
      value: 'HomeOwner',
    },
    BROKER: {
      value: 'Broker',
    },
    COMPANY: {
      value: 'Company',
		},
		USER: {
      value: 'User',
    },
  },
});

export const CreateUserInputType = new GraphQLInputObjectType({
	name: 'CreateUser',
	description: 'create user input type',
	fields: () => ({
		type: Primitives.ofType(RegisterEnumType),
		fullName: Primitives.requiredString(undefined, 'Full name'),
		province: Primitives.requiredString(undefined, 'Email'),
		phone: Primitives.string(undefined, 'Phone number'),
		email: Primitives.string(undefined, 'Email address'),
		birthDate: Primitives.string(undefined, 'Birthdate'),
		gender: Primitives.string(undefined, 'Gender'),
		password: Primitives.string(undefined, 'Password'),
	})
});

export const CreateHomeOwnerType = new GraphQLInputObjectType({
	name: 'CreateHomeOwner',
	description: 'create home owner input type',
	fields: () => ({
		fullName: Primitives.requiredString(undefined, 'full name'),
		province: Primitives.requiredString(undefined, 'Email'),
		phone: Primitives.string(undefined, 'Phone number'),
		email: Primitives.string(undefined, 'Email address'),
		birthDate: Primitives.string(undefined, 'Birthdate'),
		gender: Primitives.string(undefined, 'Gender'),
		password: Primitives.string(undefined, 'Password'),
	})
});

export const CreateBrokerType = new GraphQLInputObjectType({
	name: 'CreateBroker',
	description: 'create broker input type',
	fields: () => ({
		fullName: Primitives.requiredString(undefined, 'full name'),
		province: Primitives.requiredString(undefined, 'Email'),
		phone: Primitives.string(undefined, 'Phone number'),
		email: Primitives.string(undefined, 'Email address'),
		birthDate: Primitives.string(undefined, 'Birthdate'),
		gender: Primitives.string(undefined, 'Gender'),
		password: Primitives.string(undefined, 'Password'),
	})
});

export const CreateCompanyType = new GraphQLInputObjectType({
	name: 'CreateCompany',
	description: 'Company input type',
	fields: () => ({
		companyName: Primitives.requiredString(undefined, 'Company name'),
		province: Primitives.requiredString(undefined, 'Email'),
		phone: Primitives.string(undefined, 'Phone number'),
		email: Primitives.string(undefined, 'Email address'),
		taxId: Primitives.requiredString(undefined, 'Company tax ID'),
		establishedDate: Primitives.string(undefined, 'Established date'),
		companyType: Primitives.string(undefined, 'CompanyType'),
		password: Primitives.string(undefined, 'Password'),
	})
});


export const RegisterInputType = new GraphQLUnionType({
	name: 'RegisterInput',
	description: 'Register with 4 type: Home owner, Broker, Company, User',
	types: [CreateHomeOwnerType, CreateBrokerType, CreateCompanyType, CreateUserInputType],
	resolveType({ type }) {
		if (type === 'HomeOwner') {
			return CreateHomeOwnerType;
		} else if (type === 'Broker'){
			return CreateBrokerType;
		} else if (type === 'Company') {
			return CreateCompanyType;
		} else return CreateUserInputType;
	}
})