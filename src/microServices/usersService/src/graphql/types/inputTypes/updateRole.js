import { GraphQLBoolean, GraphQLInputObjectType, GraphQLList, GraphQLString } from 'graphql';


export const UpdateRoleInputType = new GraphQLInputObjectType({
	name: 'UpdateRole',
	description: 'Update role type',
	fields: () => ({
		description: { type: GraphQLString },
		permissions: { type: GraphQLList(GraphQLString) },
		isActive: { type: GraphQLBoolean }
	})
});
