import { GraphQLInputObjectType } from 'graphql';
import { Primitives } from '../../../../../../utils';

export const SignUpDataInputType = new GraphQLInputObjectType({
	description: 'Sign up data object',
	name: 'signUpData',
	fields: {
		email: Primitives.requiredString(),
		phone: Primitives.requiredString(),
		province: Primitives.requiredString(),
		password: Primitives.requiredString(),
		// TODO includes all fields
	}
})
