import { GraphQLInputObjectType } from 'graphql';
import { Primitives } from '../../../../../../utils';


export const UpdateUserInputType = new GraphQLInputObjectType({
	name: 'UpdateUser',
	description: 'Update user type',
	fields: () => ({
		firstName: Primitives.string(undefined, 'First name'),
		lastName: Primitives.string(undefined, 'Last name'),
		email: Primitives.string(undefined, 'Email'),
		avatar: Primitives.string(undefined, 'Avatar'),
		phone: Primitives.string(undefined, 'Phone number'),
		birthDate: Primitives.string(undefined, 'Birthdate'),
		role: Primitives.string(undefined, 'Id of role')
	})
});