import { GraphQLInputObjectType, GraphQLID, GraphQLString } from 'graphql';
import { Primitives } from '../../../../../../utils';

export const UserConditionInputType = new GraphQLInputObjectType({
	name: 'UserCondition',
	description: 'User condition',
	fields: () => ({
		id: Primitives.list(GraphQLString, null),
		email: Primitives.string(undefined, 'Search by email'),
		roleId: Primitives.string(undefined, 'Search by role'),
	})
});
