import { GraphQLUnionType } from 'graphql';
import { MessageType } from './message';
import { UserType } from './user';

export const VerifyTokenType = new GraphQLUnionType({
	name: 'VerifyToken',
	types: [MessageType, UserType],
	description: 'Define response for verify token api',
	resolveType: (obj) => {
		if(obj.message)
			return MessageType;
		else
			return UserType
	}
});
