import { GraphQLUnionType } from 'graphql';
import { AccessTokenType } from './accessToken';
import { SocialResponseDataType } from './socialResponseData';

export const SocialLoginResponseType = new GraphQLUnionType({
	name: 'SocialLoginResponse',
	types: [AccessTokenType, SocialResponseDataType],
	description: 'Define response for login with social network',
	resolveType: (obj) => {
		if(obj.accessToken)
			return AccessTokenType;
		else
			return SocialResponseDataType
	}
});
