import { GraphQLObjectType } from 'graphql';
import { Resolvers } from '../../../../../utils';

export const MessageType = new GraphQLObjectType({
	name: 'Message',
	description: ' Message',
	fields: () => ({
		message: Resolvers.string()
	})
});
