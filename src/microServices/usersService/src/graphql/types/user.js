import { GraphQLObjectType, GraphQLString, GraphQLUnionType } from 'graphql';
import { Resolvers } from '../../../../../utils';
import { permissionResolvers, roleResolversByUserRoleOrRoleId } from '../resolvers';

export const UserType = new GraphQLObjectType({
	name: 'User',
	description: 'Single User Type',
	fields: () => ({
		id: Resolvers.id(),
		firstName: Resolvers.string('User first name'),
		lastName: Resolvers.string('User last name'),
		fullName: Resolvers.string(),
		userId: Resolvers.string('User Id'),
		isPremiumAccount: Resolvers.boolean(),
		isVerifiedPhone: Resolvers.boolean(),
		isVerifiedEmail: Resolvers.boolean(),
		email: Resolvers.string(),
		deviceToken: Resolvers.string(),
		upgradePremiumDate: Resolvers.datetime(),
		avatar: Resolvers.string(),
		phone: Resolvers.string(),
		secondPhone: Resolvers.string(),
		province: Resolvers.string(),
		signupType: Resolvers.string(),
		isLocked: Resolvers.boolean(),
		skype: Resolvers.string(),
		zalo: Resolvers.string(),
		facebook: Resolvers.string(),
		viber: Resolvers.string(),
		bankAccountNumber: Resolvers.string(),
		bankName: Resolvers.string(),
		sharePersonalInfo: Resolvers.boolean(),
		shareOwnedPosts: Resolvers.boolean(),
		refMembers: Resolvers.listOfType(UserType),
		role: {
			type: RoleType,
			resolve: roleResolversByUserRoleOrRoleId,
		},
		birthDate: Resolvers.datetime(),
		createdAt: Resolvers.datetime(),
		updatedAt: Resolvers.datetime()
	})
});

export const RoleType = new GraphQLObjectType({
	name: 'Role',
	description: 'Role of user',
	fields: () => ({
		id: Resolvers.id(),
		name: Resolvers.string(),
		permissions: Resolvers.listOfType(
			PermissionType,
			'Get list of permissions',
			permissionResolvers,
		)
	})
});

export const PermissionType = new GraphQLObjectType({
	name: 'Permission',
	description: 'Get all permission of a role',
	fields: () => ({
		id: Resolvers.id(),
		name: Resolvers.string()
	})
});

export const HasPermissionType = new GraphQLObjectType({
	name: 'HasPermission',
	description: 'Return true if user has permission',
	fields: () => ({
		has: Resolvers.boolean()
	})
});

export const HomeOwnerType = new GraphQLObjectType({
	name: 'HomeOwnerType',
	description: 'Home onwer type',
	fields: () => ({
		id: Resolvers.id(),
		firstName: Resolvers.string('User first name'),
		lastName: Resolvers.string('User last name'),
		fullName: Resolvers.string(),
		userId: Resolvers.string('User Id'),
		isPremiumAccount: Resolvers.boolean(),
		isVerifiedPhone: Resolvers.boolean(),
		isVerifiedEmail: Resolvers.boolean(),
		email: Resolvers.string(),
		deviceToken: Resolvers.string(),
		upgradePremiumDate: Resolvers.datetime(),
		avatar: Resolvers.string(),
		phone: Resolvers.string(),
		secondPhone: Resolvers.string(),
		province: Resolvers.string(),
		signupType: Resolvers.string(),
		isLocked: Resolvers.boolean(),
		skype: Resolvers.string(),
		zalo: Resolvers.string(),
		facebook: Resolvers.string(),
		viber: Resolvers.string(),
		bankAccountNumber: Resolvers.string(),
		bankName: Resolvers.string(),
		sharePersonalInfo: Resolvers.boolean(),
		shareOwnedPosts: Resolvers.boolean(),
		refMembers: Resolvers.listOfType(UserType),
		role: {
			type: RoleType,
			resolve: roleResolversByUserRoleOrRoleId,
		},
		gender: Resolvers.string(),
		idNumber: Resolvers.string(),
		birthDate: Resolvers.datetime(),
		createdAt: Resolvers.datetime(),
		updatedAt: Resolvers.datetime(),
	})
});

export const BrokerType = new GraphQLObjectType({
	name: 'BrokerType',
	description: 'Broker type',
	fields: () => ({
		id: Resolvers.id(),
		firstName: Resolvers.string('User first name'),
		lastName: Resolvers.string('User last name'),
		fullName: Resolvers.string(),
		userId: Resolvers.string('User Id'),
		isPremiumAccount: Resolvers.boolean(),
		isVerifiedPhone: Resolvers.boolean(),
		isVerifiedEmail: Resolvers.boolean(),
		email: Resolvers.string(),
		deviceToken: Resolvers.string(),
		upgradePremiumDate: Resolvers.datetime(),
		avatar: Resolvers.string(),
		phone: Resolvers.string(),
		secondPhone: Resolvers.string(),
		province: Resolvers.string(),
		signupType: Resolvers.string(),
		isLocked: Resolvers.boolean(),
		skype: Resolvers.string(),
		zalo: Resolvers.string(),
		facebook: Resolvers.string(),
		viber: Resolvers.string(),
		bankAccountNumber: Resolvers.string(),
		bankName: Resolvers.string(),
		sharePersonalInfo: Resolvers.boolean(),
		shareOwnedPosts: Resolvers.boolean(),
		refMembers: Resolvers.listOfType(UserType),
		role: {
			type: RoleType,
			resolve: roleResolversByUserRoleOrRoleId,
		},
		gender: Resolvers.string(),
		idNumber: Resolvers.string(),
		company: Resolvers.string(),
		birthDate: Resolvers.datetime(),
		createdAt: Resolvers.datetime(),
		updatedAt: Resolvers.datetime(),
	})
});

export const CompanyType = new GraphQLObjectType({
	name: 'CompanyType',
	description: 'Company type',
	fields: () => ({
		id: Resolvers.id(),
		userId: Resolvers.string('User Id'),
		isPremiumAccount: Resolvers.boolean(),
		isVerifiedPhone: Resolvers.boolean(),
		isVerifiedEmail: Resolvers.boolean(),
		email: Resolvers.string(),
		deviceToken: Resolvers.string(),
		upgradePremiumDate: Resolvers.datetime(),
		avatar: Resolvers.string(),
		phone: Resolvers.string(),
		secondPhone: Resolvers.string(),
		province: Resolvers.string(),
		signupType: Resolvers.string(),
		isLocked: Resolvers.boolean(),
		skype: Resolvers.string(),
		zalo: Resolvers.string(),
		facebook: Resolvers.string(),
		viber: Resolvers.string(),
		bankAccountNumber: Resolvers.string(),
		bankName: Resolvers.string(),
		sharePersonalInfo: Resolvers.boolean(),
		shareOwnedPosts: Resolvers.boolean(),
		refMembers: Resolvers.listOfType(UserType),
		role: {
			type: RoleType,
			resolve: roleResolversByUserRoleOrRoleId,
		},
		companyName: Resolvers.string(),
		taxId: Resolvers.string(),
		idNumber: Resolvers.string(),
		isOfficial: Resolvers.string(),
		establishedDate: Resolvers.datetime(),
		description: Resolvers.string(),
		createdAt: Resolvers.datetime(),
		updatedAt: Resolvers.datetime(),
	})
});

export const UserReponseType = new GraphQLUnionType({
	name: 'RegisterInput',
	description: 'Register with 4 type: Home owner, Broker, Company, User',
	types: [HomeOwnerType, BrokerType, CompanyType, UserType],
	resolveType({ type }) {
		if (type === 'HomeOwner') {
			return HomeOwnerType;
		} else if (type === 'Broker'){
			return BrokerType;
		} else if (type === 'Company') {
			return CompanyType;
		} else return UserType;
	}
})