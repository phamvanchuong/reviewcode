import { GraphQLObjectType } from 'graphql';
import { Resolvers } from '../../../../../utils';

export const SocialResponseDataType = new GraphQLObjectType({
	name: 'SocialReponseData',
	description: 'Data when log in via social network',
	fields: () => ({
		firstName: Resolvers.string(),
		lastName: Resolvers.string(),
		email: Resolvers.string()
	})
});
