import express from 'express';
import { ApolloServer } from 'apollo-server-express';
import morgan from 'morgan';
import { graphQLSchema } from './src/graphql';
import cors from 'cors';
import bodyParser from 'body-parser';
import { i18next, i18nextMiddleware, basicAuthenticationMiddleware } from '../../utils';
import { authenticationMiddleware } from './src/middleware/authentication';
import admin from 'firebase-admin';
import firebaseServiceAccount from '../../assets/firebaseCredentials';
import '../../utils/mongoose';

const graphQlPath = `/${process.env.SERVICE_NAME}/graphql`;

const app = express();

app.use(morgan('dev'));

app.use(
	graphQlPath,
	cors(),
	bodyParser.json(),
	authenticationMiddleware,
	basicAuthenticationMiddleware,
	i18nextMiddleware.handle(i18next),
);
app.get('/', (req, res) => res.send('Hello World!'))
admin.initializeApp({
	credential: admin.credential.cert(firebaseServiceAccount),
	databaseURL: "https://sendbyshare-222308.firebaseio.com"
});
console.log('==== Initialized Firebase ====');
const server = new ApolloServer({
	schema: graphQLSchema,
	introspection: true,
	context: ({ req }) => {
		return { req };
	}
});

server.applyMiddleware({ app, path: graphQlPath });

app.listen(process.env.PORT || 1337, () => {
	console.log(`Server listen on port ${process.env.PORT || 1337}`);
});
