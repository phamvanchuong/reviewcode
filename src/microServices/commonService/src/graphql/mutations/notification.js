import moment from 'moment';
import striptasg from 'striptags';
import { GraphQLInt } from 'graphql';
import { getUserInfoById, getUsersInfoByConditions, Primitives } from '../../../../../utils';
import {
	CommonMessageType,
	NotificationType,
	SendNotiRequestType,
	ListNotificationResponseType,
} from '../types';
import {
	sendNotification,
	sendNotiWithImageUrl,
	replaceNotiBody,
} from '../../utils';
import NotificationModel from '../../models/notification';
import { notiTemplateList } from '../../template/notification/noti-template';

export const sendNoti = {
	type: CommonMessageType,
	description: 'Send notification',
	args: { data: { type: SendNotiRequestType } },
	resolve: async (rootValue, { data: { title, body, userCondition } }, { req }) => {
		try {
			const { data: { getMultiUsersInfo: users }, errors } = await getUsersInfoByConditions(userCondition);

			if (errors) {
				return Promise.reject(new Error('couldNotGetData'));
			}

			const promiseArray = [];
			users.forEach(user => {
				if (user.turnOnNotification && user.deviceToken)
					promiseArray.push(sendNotification(title, body, null, user.id))
			});
			const sendNotificationsResults = await Promise.all(promiseArray);
			if (!sendNotificationsResults.length)
				return Promise.reject(new Error(req.t('couldNotSendNoti')));
			else
				return { message: req.t('sendNotiSuccess') };
		} catch (e) {
			return Promise.reject(e);
		}
	}
};

export const listNoti = {
	type: ListNotificationResponseType,
	description: 'Get list notification',
	args: {
		page: { type: GraphQLInt, defaultValue: 1 },
		rowsPerPage: { type: GraphQLInt, defaultValue: 10 }
	},
	resolve: async (rootValue, { page, rowsPerPage }, { req }) => {
		try {
			const { user } = req,
				totalRows = await NotificationModel.countDocuments({
					toUser: user.id,
					notiType: { $nin: [1] },
					deletedAt: null
				}),
				unreadNotiCount = await NotificationModel.countDocuments({
					toUser: user.id,
					notiType: { $nin: [1] },
					isRead: false,
					deletedAt: null
				}),
				notificationList = await NotificationModel.find({
					toUser: user.id,
					notiType: { $nin: [1] },
					deletedAt: null
				}).skip((page - 1) * rowsPerPage).limit(rowsPerPage);

			return {
				page: page,
				numOfRows: notificationList.length,
				totalRows: totalRows,
				notificationList: notificationList,
				unreadNotiCount: unreadNotiCount
			}
		} catch (e) {
			return Promise.reject(e);
		}
	}
};

export const markNotiAsRead = {
	type: NotificationType,
	description: 'Mark a notification as read',
	args: {
		id: Primitives.string()
	},
	resolve: async (rootValue, { id }, { req }) => {
		try {
			const { user } = req,
				noti = await NotificationModel.findOne({
					_id: id,
					toUser: user.id,
					deletedAt: null
				});

			if (!noti) {
				return Promise.reject(new Error(req.t('notiNotFound')));
			}

			noti.isRead = true;
			await noti.save();

			return noti;
		} catch (e) {
			return Promise.reject(e);
		}
	}
};

export const removeNoti = {
	type: NotificationType,
	description: 'Remove a notification',
	args: {
		id: Primitives.string()
	},
	resolve: async (rootValue, { id }, { req }) => {
		try {
			const { user } = req,
				noti = await NotificationModel.findOne({
					_id: id,
					toUser: user.id,
					deletedAt: null
				});

			if (!noti) {
				return Promise.reject(new Error(req.t('notiNotFound')));
			}

			noti.deletedAt = moment();
			await noti.save();

			return noti;
		} catch (e) {
			return Promise.reject(e);
		}
	}
};

export const notiDetail = {
	type: NotificationType,
	description: 'Get details of a notification',
	args: {
		id: Primitives.string()
	},
	resolve: async (rootValue, { id }, { req }) => {
		try {
			const { user } = req,
				noti = await NotificationModel.findOne({
					_id: id,
					toUser: user.id,
					deletedAt: null
				});

			if (!noti) {
				return Promise.reject(new Error(req.t('notiNotFound')));
			}

			return noti;
		} catch (e) {
			return Promise.reject(e);
		}
	}
};

export const createInternalNoti = {
	type: CommonMessageType,
	description: 'Create an internal notification',
	args: {
		title: Primitives.requiredString(),
		body: Primitives.requiredString(),
		toUser: Primitives.requiredString(),
		notiType: Primitives.requiredInt(),
		imageUrl: Primitives.string()
	},
	resolve: async (rootValue, args, { req }) => {
		try {
			const	{ data: { getUserInfo: user }, error } = await getUserInfoById(args.toUser);
			if (error) {
				return Promise.reject(new Error('couldNotGetData'));
			}
			const noti = await new NotificationModel(args).save();

			if (noti && user && user.turnOnNotification && user.deviceToken) {
				const pushNotiData = {
					id: noti.id,
					notiType: args.notiType.toString(),
				};
				await sendNotiWithImageUrl(user, args.title, args.body, pushNotiData, args.imageUrl);
			}
			return { message: req.t('sendNotiSuccess') }
		} catch (e) {
			return Promise.reject(e);
		}
	}
};

export const createNotiWithTemplate = {
	type: CommonMessageType,
	description: 'Create an internal notification',
	args: {
		toUser: Primitives.requiredString(),
		template: Primitives.requiredString(),
		variables: Primitives.string()
	},
	resolve: async (rootValue, { toUser, template, variables }, { req }) => {
		try {
			variables = variables ? JSON.parse(variables) : {};
			const foundTemplate = notiTemplateList.find((element) => {
				return element.template === template;
			});

			if (!foundTemplate) {
				return Promise.reject(new Error('templateNotFound'));
			}
			let newBody = replaceNotiBody(foundTemplate.body, variables),
				newTitle = replaceNotiBody(foundTemplate.title, variables);
			newBody = newBody.replace(/\n/g, '');

			const notiData = {
				toUser: toUser,
				title: newTitle,
				body: newBody,
				notiType: foundTemplate.notiType,
				isButton: foundTemplate.isButton,
				url: foundTemplate.url
			};
			const noti = await NotificationModel.create(notiData),
				{ data: { getUserInfo: user }, error } = await getUserInfoById(toUser);
			if (error) {
				return Promise.reject(new Error('couldNotGetData'));
			}

			if (noti && user && user.turnOnNotification && user.deviceToken) {
				const pushNotiData = {
					id: noti.id,
					notiType: foundTemplate.notiType.toString(),
					url: foundTemplate.url ? foundTemplate.url : ''
				};
				await sendNotiWithImageUrl(user, newTitle, striptasg(newBody), pushNotiData, foundTemplate.imageUrl);
			}
			return { message: req.t('sendNotiSuccess') }
		} catch (e) {
			return Promise.reject(e);
		}
	}
};
