import { GraphQLObjectType } from 'graphql';
import { GraphQLString as String } from 'graphql/type/scalars';
import { middlewareResolver } from '../../../../../utils';
import { isAuthenticated, isBasicAuthenticated } from '../middleware';


import { sendSMS, sendEmail } from './common';
import {
	sendNoti,
	notiDetail,
	listNoti,
	markNotiAsRead,
	removeNoti,
	createInternalNoti,
	createNotiWithTemplate
} from './notification';

export default new GraphQLObjectType({
	name: 'Mutations',
	fields: () => ({
		[`${process.env.SERVICE_NAME}Greeting`]: greeting,
		sendSMS: middlewareResolver(sendSMS, [isBasicAuthenticated]),
		sendEmail: middlewareResolver(sendEmail, [isBasicAuthenticated]),
		sendNoti: middlewareResolver(sendNoti, [isAuthenticated]),
		notiDetail: middlewareResolver(notiDetail, [isAuthenticated]),
		listNoti: middlewareResolver(listNoti, [isAuthenticated]),
		markNotiAsRead: middlewareResolver(markNotiAsRead, [isAuthenticated]),
		removeNoti: middlewareResolver(removeNoti, [isAuthenticated]),
		createInternalNoti: middlewareResolver(createInternalNoti, [isBasicAuthenticated]),
		createNotiWithTemplate,
	}),
});

const greeting = {
	type: String,
	description: 'A warm welcome message from GraphQL, usually used to Test if the system working..',
	resolve: (rootValue, params, context) => {
		const { req } = context;
		return req.t('greetingMutation', { service: process.env.SERVICE_NAME });
	},
};
