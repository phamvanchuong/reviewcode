import { Primitives } from '../../../../../utils';
import { sendMailZoho } from '../../../../../utils';
import { publicSMSMessage } from '../../utils';
import { CommonMessageType } from '../types';

export const sendSMS = {
	type: CommonMessageType,
	description: 'Forgot password phone',
	args: {
		phone: Primitives.requiredString(),
		message: Primitives.requiredString()
	},
	resolve: async (rootValue, { phone, message }, { req }) => {
		try {
			await publicSMSMessage(phone, message);
			return { message: req.t('OTPSent') }
		} catch (e) {
			return Promise.reject(e);
		}
	}
};

export const sendEmail = {
	type: CommonMessageType,
	description: 'Send email',
	args: {
		email: Primitives.requiredString(),
		mailSubject: Primitives.requiredString(),
		mailTemplate: Primitives.requiredString(),
		variables: Primitives.string(),
		attach: Primitives.string()
	},
	resolve: async (rootValue, { email, mailSubject, mailTemplate, variables, attach}, { req }) => {
		try {
			const parsedVariables = JSON.parse(variables),
			 result = await sendMailZoho(email, mailSubject, mailTemplate, parsedVariables, attach);
			if (result)
					return { message: req.t('emailSent') };
			else
					return Promise.reject(new Error(req.t('couldNotSendEmail')));
		} catch (e) {
			return Promise.reject(e);
		}
	}
};
