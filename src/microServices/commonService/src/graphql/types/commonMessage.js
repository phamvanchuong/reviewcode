import { GraphQLObjectType } from 'graphql';
import { Resolvers } from '../../../../../utils';

export const CommonMessageType = new GraphQLObjectType({
	name: 'CommonMessage',
	description: ' Message common',
	fields: () => ({
		message: Resolvers.string()
	})
});
