import { GraphQLObjectType, GraphQLList } from 'graphql';
import { Resolvers, Primitives } from '../../../../../utils';

export const NotificationType = new GraphQLObjectType({
    name: 'Notification',
    description: 'Single Notification Type',
    fields: () => ({
        id: Resolvers.id(),
        title: Resolvers.string(),
        body: Resolvers.string(),
        toUser: Resolvers.string(),
        isRead: Resolvers.boolean(),
        notiType: Resolvers.int(),
        isButton: Resolvers.boolean(),
        url: Resolvers.string(),
        createdAt: Resolvers.datetime(),
        updatedAt: Resolvers.datetime(),
    })
});

export const ListNotificationResponseType = new GraphQLObjectType({
    name: 'ListNotificationResponse',
    description: 'Define reponse list notification',
    fields: () => ({
        page: Resolvers.int(),
        numOfRows: Resolvers.int(),
        totalRows: Resolvers.int(),
        notificationList: Primitives.list(NotificationType),
        unreadNotiCount: Resolvers.int()
    })
});

