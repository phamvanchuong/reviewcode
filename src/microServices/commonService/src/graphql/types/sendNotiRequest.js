import { GraphQLInputObjectType, GraphQLString } from 'graphql';
import { Resolvers } from '../../../../../utils';

export const UserConditionType = new GraphQLInputObjectType({
	name: 'UserCondition',
	description: 'User condition',
	fields: () => ({
		id: { type: GraphQLString },
		email: { type: GraphQLString },
		role: { type: GraphQLString }
	})
});

export const SendNotiRequestType = new GraphQLInputObjectType({
	name: 'SendNotiRequest',
	description: 'Data on body request',
	fields: () => ({
		title: { type: GraphQLString },
		body: { type: GraphQLString },
		userCondition: { type: UserConditionType }
	})
});
