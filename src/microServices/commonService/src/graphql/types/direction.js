
import { GraphQLObjectType, GraphQLInt } from 'graphql';
import { Resolvers } from '../../../../../utils';
import { LocationType } from './geocode';

export const BoundType = new GraphQLObjectType({
	name: 'Bound',
	description: 'bound type',
	fields: () => ({
		northeast: { type: LocationType },
		southwest: { type: LocationType }
	})
});

export const PolylineType = new GraphQLObjectType({
	name: 'Polyline',
	description: ' Polyline type',
	fields: () => ({
		points: Resolvers.string()
	})
});

export const KeyValueType = new GraphQLObjectType({
	name: 'KeyValue',
	description: 'Key and value type',
	fields: () => ({
		text: Resolvers.string(),
		value: Resolvers.int()
	})
});

export const StepType = new GraphQLObjectType({
	name: 'Step',
	description: 'Step type',
	fields: () => ({
		distance: { type: KeyValueType},
        duration: { type: KeyValueType},
        end_location: { type: LocationType },
        html_instructions: Resolvers.string(),
        polyline: { type: PolylineType },
        start_location: { type: LocationType },
        travel_mode: Resolvers.string(),
        maneuver: Resolvers.string()
	})
});

export const LegType = new GraphQLObjectType({
	name: 'Leg',
	description: 'Leg type',
	fields: () => ({
		distance: { type: KeyValueType},
        duration: { type: KeyValueType},
        end_location: { type: LocationType },
        start_location: { type: LocationType },
        end_address: Resolvers.string(),
        start_address: Resolvers.string(),
        steps: Resolvers.listOfType(StepType),
        traffic_speed_entry: Resolvers.stringList(),
        via_waypoint: Resolvers.stringList()
	})
});

export const RouteType = new GraphQLObjectType({
	name: 'Route',
	description: 'Route type',
	fields: () => ({
		bounds: { type: BoundType },
		copyrights: Resolvers.string(),
		legs: Resolvers.listOfType(LegType),
		overview_polyline: { type: PolylineType },
		summary: Resolvers.string(),
        warnings: Resolvers.stringList(),
        waypoint_order: Resolvers.listOfType(GraphQLInt)
	})
});

export const GeocodedWaypointType = new GraphQLObjectType({
	name: 'GeocodedWaypoint',
	description: 'GeocodedWaypoint type',
	fields: () => ({
		geocoder_status: Resolvers.string(),
        place_id: Resolvers.string(),
        types: Resolvers.stringList()
	})
});

export const DirectionResponseType = new GraphQLObjectType({
	name: 'DirectionResponseType',
	description: 'DirectionResponseType type',
	fields: () => ({
		geocoded_waypoints: Resolvers.listOfType(GeocodedWaypointType),
        routes: Resolvers.listOfType(RouteType),
        status: Resolvers.string()
	})
});