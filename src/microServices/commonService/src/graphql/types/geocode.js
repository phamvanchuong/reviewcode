import { GraphQLObjectType } from 'graphql';
import { Resolvers } from '../../../../../utils';

export const AddressComponentType = new GraphQLObjectType({
	name: 'AddressComponent',
	description: ' address component type',
	fields: () => ({
		long_name: Resolvers.string(),
		short_name: Resolvers.string(),
		types: Resolvers.stringList()
	})
});

export const LocationType = new GraphQLObjectType({
	name: 'Location',
	description: ' Location type',
	fields: () => ({
		lat: Resolvers.float(),
		lng: Resolvers.float()
	})
});

export const ViewportType = new GraphQLObjectType({
	name: 'Viewport',
	description: 'Viewport type',
	fields: () => ({
		northeast: { type: LocationType },
		southwest: { type: LocationType }
	})
});

export const PlusCodeType = new GraphQLObjectType({
	name: 'PlusCode',
	description: 'Plus code type',
	fields: () => ({
		compound_code: Resolvers.string(),
		global_code: Resolvers.string()
	})
});

export const GeometryType = new GraphQLObjectType({
	name: 'Geomettry',
	description: 'Geomettry type',
	fields: () => ({
		location: {type: LocationType },
		location_type: Resolvers.string(),
		viewport: { type: ViewportType }
	})
});

export const ResultsType = new GraphQLObjectType({
	name: 'Results',
	description: 'Result type',
	fields: () => ({
		address_components: Resolvers.listOfType(AddressComponentType),
		formatted_address: Resolvers.string(),
		geometry: { type: GeometryType },
		place_id: Resolvers.string(),
		plus_code: { type: PlusCodeType },
		types: Resolvers.stringList()
	})
});

export const GeocodeResultType = new GraphQLObjectType({
	name: 'GeocodeResult',
	description: 'Geocode result type',
	fields: () => ({
		results: Resolvers.listOfType(ResultsType),
		status: Resolvers.string()
	})
});
