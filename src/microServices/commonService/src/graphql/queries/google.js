import { GeocodeResultType, DirectionResponseType } from '../types';
import { Primitives } from '../../../../../utils';
import { googleGetAPI } from '../../utils';


export const googleGeocode = {
	type: GeocodeResultType,
	description: 'Get Geocode',
	args: {
		address: Primitives.requiredString(),
		clientKey: Primitives.requiredString()
	},
	resolve: async (root, args, { req }) => {
		try {
			if (args.clientKey !== process.env.APPKEY) {
				return Promise.reject(new Error(req.t('clientKeyIsIncorrect')));
			}
			args.key = process.env.GOOGLE_API_KEY;
			return await googleGetAPI(process.env.GOOGLE_GEOCODE_URL, args);
		} catch (error) {
			return Promise.reject(error);
		}
	},
};

export const googleDirections = {
	type: DirectionResponseType,
	description: 'Get direction',
	args: {
		origin: Primitives.requiredString(),
		destination: Primitives.requiredString(),
		waypoints: Primitives.string(),
		clientKey: Primitives.requiredString()
	},
	resolve: async (root, args, { req }) => {
		try {
			if (args.clientKey !== process.env.APPKEY) {
				return Promise.reject(new Error(req.t('clientKeyIsIncorrect')));
			}
			args.key = process.env.GOOGLE_API_KEY;
			return await googleGetAPI(process.env.GOOGLE_DIRECTION_URL, args);
		} catch (error) {
			return Promise.reject(error);
		}
	},
};
