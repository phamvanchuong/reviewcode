export function replaceNotiBody(body, variables) {
	let newBody = body;
	const keys = Object.keys(variables);

	for (let iterator of keys) {
		newBody = newBody.replace(`{{${iterator}}}`, variables[iterator])
	}
	return newBody;
}
