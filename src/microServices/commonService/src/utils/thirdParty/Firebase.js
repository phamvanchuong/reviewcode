import admin from 'firebase-admin';

export function noti(message) {
    return admin.messaging().send(message);
}
