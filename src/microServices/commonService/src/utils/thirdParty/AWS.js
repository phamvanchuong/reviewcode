import AWS from 'aws-sdk';

AWS.config.update({
    region: 'ap-southeast-1',
    accessKeyId: process.env.AWS_ACCESS_KEY_ID,
    secretAccessKey: process.env.AWS_SECRET_ACCESS_KEY
});
/**
 * Amazon service
 *
 *
 */

/**
 * Public sms message
 *
 * @see: https://docs.aws.amazon.com/sdk-for-javascript/v2/developer-guide/sns-examples-sending-sms.html
 */
export function publicSMSMessage(phone, message) {
    // Create publish parameters
    const params = {
        Message: message,// 'TEXT_MESSAGE', /* required */
        PhoneNumber: phone, // 'E.164_PHONE_NUMBER',
    };

    // Handle promise's fulfilled/rejected states
    return new AWS.SNS({ apiVersion: '2010-03-31' }).publish(params).promise();
}

/**
 * Public sms message
 *
 * @see: https://docs.aws.amazon.com/sdk-for-javascript/v2/developer-guide/sns-examples-sending-sms.html
 */
export function get_sms_attributes(){

    const params = {
        attributes: [
            'DefaultSMSType',
            'DefaultSenderID'
            /* more items */
        ]
    };
    // Create promise and SNS service object
    let getSMSTypePromise = new AWS.SNS({ apiVersion: '2010-03-31' }).getSMSAttributes(params).promise();

    // Handle promise's fulfilled/rejected states
    getSMSTypePromise.then( (data) => {
        console.log(data);
    }).catch( (err) => {
        console.error(err, err.stack);
    });

}

/**
 * Public sms message
 *
 * @see: https://docs.aws.amazon.com/sdk-for-javascript/v2/developer-guide/sns-examples-sending-sms.html
 */
export function set_sms_attributes(){
    const params = {
        attributes: { /* required */
            'DefaultSMSType': 'Transactional', /* highest reliability */
            //'DefaultSMSType': 'Promotional', /* lowest cost */
            'DefaultSenderID': 'SendByShare'
        }
    };

    // Create promise and SNS service object
    let setSMSTypePromise = new AWS.SNS({ apiVersion: '2010-03-31' }).setSMSAttributes(params).promise();

    // Handle promise's fulfilled/rejected states
    setSMSTypePromise.then( (data) => {
        console.log(data);
    }).catch( (err) => {
        console.error(err, err.stack);
    });

}
