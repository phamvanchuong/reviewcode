import fetch from 'node-fetch';

export async function googleGetAPI(url, data) {
    const getURL = new URL(url);
    getURL.search = new URLSearchParams(data).toString();
    return await (await fetch(getURL)).json();
}
