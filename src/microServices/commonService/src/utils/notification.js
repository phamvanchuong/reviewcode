import moment from 'moment';
import { NotificationModel } from '../models/notification';
import { noti } from '../utils';
import { getUserInfoById } from '../../../../utils/serviceCommunicationGraphQL/queries';

export async function createNotification(notiData, pushNotiData, userCondition) {

    try {
        notiData.title = notiData.title + ` at ${moment()}`;
        notiData.newTitle = notiData.newTitle + ` at ${moment()}`;

        const createdNoti = await new NotificationModel(notiData).save(),
          foundToUser = await getUserInfoById(notiData.toUser);

        if (createdNoti && foundToUser.turnOnNotification && foundToUser.deviceToken) {

            let data = {
                id: createdNoti.id
            };
            return sendNotification(pushNotiData.title, pushNotiData.body, data, pushNotiData.toUser);
        }
    } catch (error) {
        return error;
    }
}


export async function sendNotification(title, body, data, userId) {

    try {
        const { data: { getUserInfo: user } } = await getUserInfoById(userId);

        if (!user) {
            return Promise.reject(new Error('User not found'));
        }

        const message = {
            data: data || {
                score: '850',
                time: '2:45'
            },
            notification: {
                title: title || 'Send by Share',
                body: body || 'Send by Share notification',
            },
            android: {
                ttl: 3600 * 1000, // 1 hour in milliseconds
                priority: 'high',
                notification: {
                    title: title || 'Send by Share',
                    body: body || 'Send by Share notification',
                    sound: 'default'
                }
            },
            apns: {
                headers: {
                    'apns-priority': '10'
                },
                payload: {
                    aps: {
                        alert: {
                            title: title || 'Send by Share',
                            body: body || 'Send by Share notification',
                            sound: 'default'
                        },
                        badge: 1,
                    }
                }
            },
            token: user.deviceToken
        };

        // Send a message to devices subscribed to the provided topic.
        return noti(message);
    } catch (error) {
        return error
    }

}

export function sendNotiWithImageUrl(toUser, title, body, data, imageUrl) {

    const message = {
        data: data || {
            score: '850',
            time: '2:45'
        },
        notification: {
            title: title || 'Send by Share',
            body: body || '', //'Send by Share notification',
            image: imageUrl
        },
        android: {
            ttl: 3600 * 1000, // 1 hour in milliseconds
            priority: 'high',
            notification: {
                title: title || 'Send by Share',
                body: body || '', //'Send by Share notification',
                sound: 'default',
                image: imageUrl
            }
        },
        apns: {
            headers: {
                'apns-priority': '10'
            },
            payload: {
                aps: {
                    alert: {
                        title: title || 'Send by Share',
                        body: body || 'Send by Share notification',
                        sound: 'default'
                    },
                    badge: 1,
                }
            }
        },
        token: toUser.deviceToken
    };

    // Send a message to devices subscribed to the provided topic.
    return noti(message)
}
