/**
 * Noti template list
 *
 * @author: Tam Tran
 */

export const notiTemplateList = [
    // Batch jobs
    {
        title: 'Hey SBS Greener, are you working tomorrow?',
        body: 'Without confirmation, you will not receive orders to deliver tomorrow 😓',
        notiType: 1,
        template: 'confirm-working',
        imageUrl: null
    },
    {
        title: 'Congrats! Your courier account has been approved',
        body: 'Below is the list of approved information ✅✅✅: <br>\n'
        + '{{approvedItems}}<br>\n'
        + 'Now, you can accept orders in Order Market'
        ,isButton:true,
        notiType: 2,
        template: 'approve-shipper2',
        imageUrl: null
    },
    {
        title: 'Congrats! Your courier account has been approved',
        body: 'Welcome to the SBS family. Now you are officially a green courier 🌱.<br>\n'
        + 'It\'s time to set up your work schedule 📅 and bank information 🏦 to receive payments from SBS.'
        ,isButton:true,
        notiType: 4,
        template: 'approve-shipper',
        imageUrl: null
    },
    {
        title: 'Your account requires modifications to be approved',
        body: 'Please go to "My Courier Information" to update the "rejected" information.'
        ,isButton:true,
        notiType: 5,
        template: 'reject-shipper',
        imageUrl: null
    },
    {
        title: 'Cancellation request of order {{orderId}} has been approved',
        body: 'We\'re so sorry for the inconvenience 😓😓😓.',
        notiType: 6,
        template: 'admin-cancel-order',
        imageUrl: null
    },
    {
        title: 'Order {{orderId}} has been canceled by sender',
        body: '"Sender {{senderName}} has just canceled order {{orderId}}<br>\n'
        + 'Reason: {{cancelReason}}',
        notiType: 7,
        template: 'sender-cancel-order',
        imageUrl: null
    },
    // Batch job
    {
        title: 'Update your information and start making money now',
        body: '',
        notiType: 8,
        template: 'ask-finish-phase2',
        imageUrl: null
    },

    {
        title: 'We need you. Please come back.',
        body: 'So many orders 📦📦📦 are waiting to be picked up by green couriers like you. <br>\n'
        + 'Check out the Order Market and make some money now 💰💰💰.',
        notiType: 9,
        template: 'ask-go-back',
        imageUrl: null
    },
    {
        title: 'Your shift will start in 15 minutes. Be prepared and enjoy the ride',
        body: '',
        notiType: 10,
        template: 'before-shift',
        imageUrl: null
    },
    {
        title: 'Your shift started 15 minutes ago. Hurry up and join SBS\'s green delivery team!',
        body: '',
        notiType: 11,
        template: 'after-shift',
        imageUrl: null
    },
    {
        title: 'We haven\'t seen you at work today',
        body: 'Did you forget to go to work today 😔?<br>\n'
        + 'Not working during confirmed working days will result in lower chances of getting orders in the future. Also remember to turn on 3G while working.<br>\n'
        + 'If you have troubles using the app, please contact our customer service 📞 at +31 657469272.<br>\n'
        + 'If you need to update your work schedule, click the button below.'
        ,isButton:true,
        notiType: 12,
        template: 'right-after-shift',
        imageUrl: null
    },
    {
        title: 'We haven\'t seen you at work today',
        body: 'Did you forget to go to work today 😔?<br>\n'
        + 'Not working during confirmed working days will result in lower chances of getting orders in the future. Also remember to turn on 3G while working.<br>\n'
        + 'If you have troubles using the app, please contact our customer service 📞 at +31 657469272.<br>\n'
        + 'If you need to update your work schedule, click the button below.'
        ,isButton:true,
        notiType: 13,
        template: 'flexible-confirm-not-work',
        imageUrl: null
    },
    {
        title: 'Explore how to make money with SBS app',
        body: 'Dear green courier 🚵,<br>\n'
        + 'Welcome on-board to our SBS family 💖<br>\n'
        + 'We are more than excited to start the green delivery journey with you. Before we get started, let’s take a tour around the app and learn how to navigate it.<br>\n',
        notiType: 14,
        template: 'first-day-noti',
        imageUrl: null
    },
    {
        title: 'Orders are waiting for you!',
        body: 'So many packages 📦📦📦 are waiting to be picked up!<br>\n'
        + 'Check out the <a href=order-market><b>Order Market</b></a>  now for more details.<br>\n'
        + 'Don’t forget to <a href=work-schedule><b>set up your work schedule</b></a> ⏰ to start accepting orders.'

        ,isButton:true,
        notiType: 15,
        template: 'second-day-noti',
        imageUrl: null
    },
    {
        title: 'Did you know that setting up a fixed schedule can earn you more money?',
        body: '🕐 When you tell us ahead of time your working shifts, we can assign you the best delivery routes that not only suit your schedule but also result in more income 💰<br>\n'
        + '📅 Go to My Work Schedule and test it out now!'
        ,isButton:true,
        notiType: 16,
        template: 'third-day-noti',
        imageUrl: null
    },
    {
        title: 'Tell us your experience with SBS app',
        body: 'Welcome back to SBS! How has your user experience been so far?<br>\n'
        + 'Your opinion matters to us 💝. Tell us everything in the Feedback tab now.'
        ,isButton:true,
        notiType: 17,
        template: 'fourth-day-noti',
        imageUrl: null
    },
    {
        title: 'Join the SBS Courier community on Facebook',
        body: 'Share your experience and have your questions answered in the '
        +'<a href=https://www.facebook.com/groups/2422990058027500/><b>SBS Couriers Group on Facebook</b></a> 🧑‍🤝‍🧑.<br>\n',
        notiType: 18,
        template: 'fifth-day-noti',
        imageUrl: null,
        url: 'https://www.facebook.com/groups/2422990058027500/'
    },
    {
        title: 'Accept your first order now!',
        body: 'Your account has been approved ✅.<br>\n'
        + 'Your first order is waiting to be accepted in the Order Market.<br>\n'
        + '🍀 Best of luck with your first green delivery!'
        ,isButton:true,
        notiType: 19,
        template: 'first-order-announcement',
        imageUrl: null
    },
    {
        title: 'Great job on your first order!',
        body: 'You did a wonderful job with the first delivery 😍.<br>\n'
        + 'Now you are officially a green courier 🚵.<br>\n'
        + 'Keep up the good work! 😉',
        notiType: 20,
        template: 'first-order-completion',
        imageUrl: null
    },

    {
        title: 'Why use green vehicles?',
        body: '"👉 Conventional cars release approximately 333 million tons of carbon dioxide 💨 into the atmosphere annually, '
        + 'which is 20 percent of the world\'s total, according to the Environmental Defense Fund.<br>\n'
        + '👉 Travel and deliver with your e-vehicles to reduce air pollution 🚵.',
        notiType: 21,
        template: 'weekly-friday-one',
        imageUrl: null
    },
    {
        title: 'How much C02 do cars release?',
        body: 'Need one more reason to choose e-vehicles over conventional cars 🚘?<br>\n'
        + '👉 Burning one gallon of gas 💨 creates 20 pounds of carbon dioxide, and the average car emits about six tons of carbon dioxide every year. <br>\n'
        + 'Make your call 😉',
        notiType: 22,
        template: 'weekly-friday-two',
        imageUrl: null
    },
    {
        title: 'Check out your account balance',
        body: 'Have a look at your income 💸 this month under <a href=shipment-history><b>My Shipment History.</b></a><br>\n'
        + 'Don’t forget to also update your bank account information 💳 under <a href=courier-info><b>My Courier Information.</b></a><br>\n'
        + '💲 Payment is transferred to you by the 5th of each month.'

        ,isButton:true,
        notiType: 23,
        template: 'weekly-friday-three',
        imageUrl: null
    },
    {
        title: 'Refer friends to get SBS tokens',
        body: '👦 Refer a friend to become an SBS courier and get an SBS token for every successful reference.<br>\n'
        + '🤑 SBS tokens can be exchanged for money or SBS shares.<br>\n'
        + 'Once your friend\'s courier account is approved, send an email (📧: <a href=mailto:name@email.com><b>info@sendbyshare.com</b></a>) to us with both of your courier ID numbers. '
        + 'We will follow up by issuing an SBS token to the referrer.',
        notiType: 24,
        template: 'weekly-friday-four',
        imageUrl: null
    },
    {
        title: '6 Best Environmentally Friendly Cars Of 2019',
        body: '🚙 Looking to buy an environmentally friendly car? Here are the top picks for 2019:<br>\n'
        + '1 Nissan Leaf<br>\n'
        + '2 Mercedes-Benz 350e Plug-in hybrid<br>\n'
        + '3 Tesla model 3<br>\n'
        + '4 Toyota Prius Two Eco<br>\n'
        + '5 Hyundai Ioniq<br>\n'
        + '6 BMW i3<br>\n<br>\n'
        + 'Source: <a href=https://methodshop.com/2019/01/environmentally-friendly-cars.shtml><b>methodshop.com</b></a><br>\n',
        notiType: 25,
        template: 'weekly-friday-five',
        imageUrl: null,
        url: 'https://methodshop.com/2019/01/environmentally-friendly-cars.shtml'
    },
    {
        title: 'What is green delivery?',
        body: 'Green delivery 🚵 is choosing the most efficient way to transport items, '
        + 'whether that means grouping deliveries together 📦📦📦, '
        + 'waiting until the delivery vehicle is at full capacity before sending it out, '
        + 'or using carbon-efficient transport such as bikes 🚲 or electric vehicles 🚕.',
        notiType: 26,
        template: 'weekly-friday-six',
        imageUrl: null
    },
    {
        title: 'Why is SBS a green delivery solution ',
        body: 'SBS groups orders together in a way that optimizes delivery efficiency 📦📦📦<br>\n'
        + 'We prioritize couriers that use eco-friendly vehicles 🚵🚵🚵<br>\n'
        + 'We provide retailers with more shipment options other than conventional delivery vans 🚚🚚🚚 ',
        notiType: 27,
        template: 'weekly-friday-seven',
        imageUrl: null
    },
    {
        title: 'Top 5 countries using electric cars in 2019',
        body: '🇳🇱The Netherlands is among the top 5 countries worldwide that use electric cars in 2019. <br>\n'
        + '1- Norway (49.10%)<br>\n'
        + '2- Iceland (19%)<br>\n'
        + '3- Sweden (8.20%)<br>\n'
        + '4- Netherlands (6.50%)<br>\n'
        + '5- Andorra (5.60%)<br>\n'
        + 'Source: <a href=https://avtowow.com/countries-by-electric-car-use><b>avtowow.com</b></a>',
        notiType: 28,
        template: 'weekly-friday-eight',
        imageUrl: null,
        url: 'https://avtowow.com/countries-by-electric-car-use'
    },
    {
        title: 'The best electric bikes you can buy',
        body: 'Looking to buy a new e-bike 🚲 for your next green delivery? Check out the top 5 e-bikes of 2019:<br>\n'
        + '1. Gocycle gx<br>\n'
        + '2. Brompton electric<br>\n'
        + '3. Volt pulse<br>\n'
        + '4. Vanmoof electrified s2<br>\n'
        + '5. Gtech ebike city<br>\n'
        + 'Source: <a href=https://www.t3.com/features/best-electric-bike><b>t3.com</b></a><br>\n',
        notiType: 29,
        template: 'weekly-friday-nine',
        imageUrl: null
    },
    {
        title: 'What are green vehicles?',
        body: '🚲 Green vehicles include vehicle types that function fully or partly on alternative energy sources other than fossil fuel or less carbon intensive than gasoline or diesel.<br>\n'
        + '🚗 Another option is the use of alternative fuel composition in conventional fossil fuel-based vehicles, making them function partially on renewable energy sources.',
        notiType: 30,
        template: 'weekly-friday-ten',
        imageUrl: null
    },
    {
        title: 'Learn more about human-powered transport',
        body: '🚲🚶 Human powered transport includes walking, bicycles, velomobiles, row boats, and other environmentally friendly ways of getting around. In addition to the health benefits of the exercise provided, they are far more environmentally friendly than most other options 👍. ',
        notiType: 31,
        template: 'weekly-friday-eleven',
        imageUrl: null
    },
    {
        title: 'Why shouldn’t we use conventional vehicles?',
        body: 'Vehicle pollutants have been linked to human ill health including the incidence of respiratory and cardiopulmonary disease and lung cancer. <br>\n'
        + '👉 According to the World Health Organization, up to 13,000 deaths per year among children (aged 0–4 years) across Europe are directly attributable to outdoor pollution. <br>\n'
        + 'The organization estimates that if pollution levels were returned to within EU limits, more than 5,000 of these lives could be saved each year.',
        notiType: 32,
        template: 'weekly-friday-twelve',
        imageUrl: null
    },
    {
        title: 'Learn more about tax incentives for e-vehicles',
        body: '💡 As of April 2010, 15 of the 27 member states of the European Union provide tax incentives for electrically chargeable vehicles and some alternative fuel vehicles, which includes all Western European countries except Italy and Luxembourg, plus the Czech Republic and Romania. <br>\n'
        + '👉 The incentives consist of tax reductions and exemptions, as well as of bonus payments for buyers of electric cars, plug-in hybrids, hybrid electric vehicles and natural gas vehicles.',
        notiType: 33,
        template: 'weekly-friday-thirteen',
        imageUrl: null
    },
    {
        title: 'Green Car of the Year” from 2011-2017',
        body: '🎖 The following vehicles won the “Green Car of the Year” award from 2011-2017:<br>\n'
        + '🌟 Chevrolet Bolt EV — 2017 Award<br>\n'
        + '🌟 Chevrolet Volt (second generation) — 2016 Award<br>\n'
        + '🌟 BMW i3 — 2015 Award<br>\n'
        + '🌟 Honda Accord ninth generation line-up — 2014 Award<br>\n'
        + '🌟 Ford Fusion 2nd gen line-up — 2013 Award<br>\n'
        + '🌟 Honda Civic GX — 2012 Award<br>\n'
        + '🌟 Chevrolet Volt — 2011 Award',
        notiType: 34,
        template: 'weekly-friday-fourteen',
        imageUrl: null
    },
    {
        title: 'E-car vs petrol car performance',
        body: '👉 For every 100km travelled in a petrol car 🚘, it takes 26 megajoules to get petrol out of the ground and transport it to the car, and the car itself uses 142 megajoules to move itself around.<br>\n'
        + '👉 For the same distance in an electric car 🚗, using electricity generated in an oil-fired power plant, it takes 74 megajoules to generate and transport the electricity to the car, which then uses just 38 megajoules to move itself and its passengers.<br>\n<br>\n'
        + 'Source: <a href=https://www.theguardian.com/football/ng-interactive/2017/dec/25/how-green-are-electric-cars><b>theguardian.com</b></a>',
        notiType: 35,
        template: 'weekly-friday-fifteen',
        imageUrl: null,
        url: 'https://www.theguardian.com/football/ng-interactive/2017/dec/25/how-green-are-electric-cars'
    },
    {
        title: 'Number of e-vehicles to reach 125 million by 2030 worldwide',
        body: '🚗 The number of electric vehicles on the road around the world will hit 125 million by 2030, the International Energy Agency forecasts. That marks a big jump from 2017, when the IEA estimated there were 3.1 million electric vehicles in use, up 54 percent from the previous year.<br>\n'
        + '🚘 IEA’s outlook still leaves plenty of room for fossil fuel-powered vehicles. Forecasts put the world’s total car count at roughly 2 billion somewhere in the 2035 to 2040 window.<br>\n'
        + 'Source: <a href=cnbc.com><b>cnbc.com</b></a>',
        notiType: 36,
        template: 'weekly-friday-sixteen',
        imageUrl: null,
        url: 'cnbc.com'
    },
    {
        title: 'Salary transferred successfully',
        body: 'This month income is {{salaryCount}} 💰💰💰 .'
        + 'More orders you deliver, more money that you are going to make<br>\n'
        + 'Note: salary is transferred by {{dayOfMonth}} of each month',
        notiType: 50,
        template: 'salary-announce',
        imageUrl: null
    },
    {
        title: 'Your schedule is changed!',
        body: 'Your schedule on {{date}} is changed from {{oldStatus}} to {{newStatus}} by SBS Admin.<br>\nIf you find any mistake, please contact SBS’s courier<br>\nservice at {{phoneNumber}} or send your issue to {{email}}',
        notiType: 51,
        template: 'schedule-approve-announce',
        imageUrl: null
    },
    {
        title: 'Your schedule is changed!',
        body: 'Your schedule on {{date}} is changed to {{status}} by SBS Admin.<br>\nIf you find any mistake, please contact SBS’s courier service<br>\nat {{phoneNumber}} or send your issue to {{email}}',
        notiType: 52,
        template: 'schedule-approve-announce-new',
        imageUrl: null
    }
];
