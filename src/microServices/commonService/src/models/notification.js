import mongoose, { Schema } from 'mongoose';

const notificationSchema = new Schema({
	title: String,
	body: String,
	toUser: String,
	isRead:  { type: Boolean, default: false },
	notiType: Number,
	isButton: { type: Boolean, default: false },
	url: String,
	deletedAt: { type: Date, default: null },
}, { timestamps: true });

const NotificationModel = mongoose.model('Notification', notificationSchema);

export default NotificationModel;
