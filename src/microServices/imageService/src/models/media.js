import mongoose, { Schema } from 'mongoose';

const urlSchema = new Schema({
	location: { type: String, required: true },
	key: { type: String, required: true },
})

const mediaSchema = new Schema({
	type: { type: String, enum: ['image', 'video'], required: true },
	filepath: { type: String, required: true },
	transcodeStatus: { type: String, enum: ['pending', 'transcoding', 'transcoded'], required: true, default: 'pending' },
	urls: { type: Map, of: urlSchema, default: new Map() },
	isDeleted: { type: Boolean, default: false },
	deletedAt: { type: Date },
}, { timestamps: true });

export const MediaModel = mongoose.model('Media', mediaSchema);
