import { AuthenticationError } from 'apollo-server-express';

export const isAuthenticated = (root, args, context) => {
	if (!context.req.user) {
		return new AuthenticationError('Not authenticated')
	}
};

export const isBasicAuthenticated = (root, args, context) => {
	if (!context.req.basicAuthenticated) {
		return new AuthenticationError('Not authenticated')
	}
};
