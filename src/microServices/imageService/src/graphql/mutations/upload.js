import { GraphQLUpload } from 'apollo-server-express';
import { GraphQLNonNull, GraphQLList } from 'graphql';
import { UploadResponseType, SingleUploadResponseType } from '../types';
import { uploadImageToS3 } from '../../utils/helpers';
import { Primitives } from '../../../../../utils';

// TODO add fileType to allow image and video

export const singleUpload = {
	type: SingleUploadResponseType,
	description: 'Upload single file',
	args: {
		file: Primitives.ofType(GraphQLNonNull(GraphQLUpload), undefined, 'File to upload')
	},
	resolve: async (rootValue, { file }, { req }) => {
		try {
			const { fileId, urls } = await uploadImageToS3(file, 'image');
			return {
				message: req.t('Upload success'),
				file: { fileId, urls }
			}
		} catch (e) {
			return Promise.reject(e);
		}
	}
};

export const multipleUpload = {
	type: UploadResponseType,
	description: 'Upload multiple files',
	args: {
		files: { type: GraphQLNonNull(GraphQLList(GraphQLNonNull(GraphQLUpload))) }
	},
	resolve: () => {}
};
