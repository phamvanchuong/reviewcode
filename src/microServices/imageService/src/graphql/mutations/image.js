import { Primitives } from '../../../../../utils';
import { RemoveImageInputType, FileType } from '../types';
import { removeImagesFromS3 } from '../../utils';

export const removeImages = {
	type: FileType,
	description: 'Remove images',
	args: {
		imageKeys: Primitives.requiredList(RemoveImageInputType)
	},
	resolve: async (rootValue, { imageKeys }, { req }) => {
		try {
			return await removeImagesFromS3(imageKeys);
		} catch (e) {
			return Promise.reject(e);
		}
	}
};