import { GraphQLObjectType } from 'graphql';
import { GraphQLString as String } from 'graphql/type/scalars';
import { isBasicAuthenticated, isAuthenticated } from '../middleware';
import { middlewareResolver } from '../../../../../utils';
import { singleUpload, multipleUpload } from './upload';
import { removeImages } from './image';

export default new GraphQLObjectType({
	name: 'Mutations',
	fields: () => ({
		singleUpload: middlewareResolver(singleUpload, [isAuthenticated]),
		multipleUpload: middlewareResolver(multipleUpload, [isAuthenticated]),
		removeImages: middlewareResolver(removeImages, [isAuthenticated])
	}),
});

const greeting = {
	type: String,
	description: 'A warm welcome message from GraphQL, usually used to Test if the system working..',
	resolve: (rootValue, params, context) => {
		const { req } = context;
		return req.t('greetingMutation', { service: process.env.SERVICE_NAME });
	},
};
