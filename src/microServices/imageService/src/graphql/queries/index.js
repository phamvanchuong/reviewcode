import { GraphQLObjectType, GraphQLString as String, } from 'graphql';
import { readMedia } from './media';
import { middlewareResolver } from '../../../../../utils';
import { isBasicAuthenticated } from '../middleware';

export default new GraphQLObjectType({
	name: 'Queries',
	fields: () => ({
		[`${process.env.SERVICE_NAME}Greeting`]: greeting,
		readMedia: middlewareResolver(readMedia, [isBasicAuthenticated]),
	}),
});

const greeting = {
	type: String,
	description: 'A warm welcome message from GraphQL, usually used to Test if the system working..',
	resolve: (rootValue, params, context) => {
		const { req } = context;
		return req.t('greetingQuery', { service: process.env.SERVICE_NAME });
	},
};
