import { UserInputError } from 'apollo-server-express';
import { FileType } from '../types';
import { getMediaRedisKey, Primitives, redisClient } from '../../../../../utils';
import { MediaModel } from '../../models/media';

export const readMedia = {
	type: FileType,
	description: 'Read file data',
	args: {
		fileId: Primitives.requiredString('File Id to read')
	},
	resolve: async (rootValue, { fileId }, { req }) => {
		const data = await redisClient.get(getMediaRedisKey(fileId));
		if (data) {
			return { fileId, urls: JSON.parse(data) }
		}
		try {
			const fileData = await MediaModel.findOne({ _id: fileId });
			if (!fileData) {
				return new UserInputError(req.t('notFoundFileId'));
			}
			await redisClient.set(getMediaRedisKey(fileId), JSON.stringify(fileData.urls));
			return {
				fileId,
				urls: fileData.urls
			}
		} catch (e) {
			return Promise.reject(e);
		}
	}

}
