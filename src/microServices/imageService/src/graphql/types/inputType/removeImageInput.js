import { GraphQLInputObjectType } from 'graphql';
import { Primitives } from '../../../../../../utils';

export const RemoveImageInputType = new GraphQLInputObjectType({
	name: 'RemoveImageInput',
	description: 'list key of image',
	fields: () => ({
		Key: Primitives.requiredString('Key of image'),
	})
});