import { GraphQLObjectType, GraphQLString, GraphQLID, GraphQLList, GraphQLScalarType } from 'graphql';

export const FileType = new GraphQLObjectType({
  name: 'File',
  description: 'A stored file.',
  fields: () => ({
    fileId: {
      type: GraphQLID,
      description: 'File ID in Database'
    },
    urls: {
      type: new GraphQLScalarType({
        name: 'url',
        serialize: value => value
      }),
      description: 'url of media file'
    }
  }),
});

export const SingleUploadResponseType = new GraphQLObjectType({
  name: 'SingleUpLoadResponse',
  description: 'file upload result',
  fields: () => ({
    message: {
      description: 'Upload message',
      type: GraphQLString
    },
    file: {
      description: 'File result',
      type: FileType
    }
  })
})

export const UploadResponseType = new GraphQLObjectType({
  name: 'UploadResponse',
  description: 'Return success and failed uploaded files.',
  fields: () => ({
    success: {
      description: 'Filename, including extension.',
      type: GraphQLList(FileType),
    },
    failure: {
      description: 'List of failed uploads.',
      type: GraphQLList(FailedUploadType),
    },
  }),
});

export const FailedUploadType = new GraphQLObjectType({
  name: 'FailedUpload',
  description: 'Return failed message and file name.',
  fields: () => ({
    error: {
      description: 'Error and message.',
      type: GraphQLString,
    },
    filename: {
      description: 'File name',
      type: GraphQLString,
    },
  }),
});
