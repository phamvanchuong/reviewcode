import AWS from 'aws-sdk';

export const s3 = new AWS.S3({
  accessKeyId: process.env.AWS_ACCESS_KEY_ID,
  secretAccessKey: process.env.AWS_SECRET_ACCESS_KEY
})

export const removeImagesFromS3 = async (imageKeyList) => {
   const deleteParams = {
       Bucket: process.env.S3_BUCKET_NAME,
       Delete: {
           Objects: imageKeyList
       }
   };
  const result =  await s3.deleteObjects(deleteParams).promise()
  console.log(result);
  return result;
}
