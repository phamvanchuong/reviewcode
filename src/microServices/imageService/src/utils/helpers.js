import shortid from 'shortid';
import _ from 'lodash';
import { s3 } from './thirdParties';
import { MediaModel } from '../models/media';
import { getMediaRedisKey, redisClient } from '../../../../utils';


export const uploadImageToS3 = async (file, filepath) => {
	const { createReadStream, filename, mimetype } = await file,
		stream = createReadStream(),
		id = shortid.generate();
	if (!_.includes(mimetype, 'image')) {
		return Promise.reject({ error: 'Only image type support.', filename });
	}
	// Minify size of image
	// const result = compressImage(filePath, process.env.COMPRESS_OUTPUT_PATH);

	// Upload file to S3
	const uploadPath = filepath ? `${filepath}/tncc-${id}-${filename}` : `tncc-${id}-${filename}`;

	const uploadParams = {
		Bucket: process.env.S3_BUCKET_NAME,
		Key: uploadPath,
		Body: stream,
		ACL: 'public-read',
		ContentType: mimetype
	};
	try {
		const { Location, Key } = await s3.upload(uploadParams).promise();
		const media = new MediaModel({
				type: 'image',
				filepath: uploadPath,
			});
		media.urls.set('original', { location: Location, key: Key, });
		await media.save();
		await redisClient.set(getMediaRedisKey(media.id), JSON.stringify(media.urls));
		return { fileId: media.id, urls: media.urls };
	} catch (error) {
		console.log(error);
		throw error;
	}
}
