import express from 'express';
import { ApolloServer } from 'apollo-server-express';
import morgan from 'morgan';
import { graphQLSchema } from './src/graphql';
import cors from 'cors';
import bodyParser from 'body-parser';
import {
	i18next,
	i18nextMiddleware,
	basicAuthenticationMiddleware,
	otherServicesAuthenticationMiddleware
} from '../../utils';
import '../../utils/mongoose';

const graphQlPath = `/${process.env.SERVICE_NAME}/graphql`;

const app = express();

app.use(morgan('dev'));
app.get('/', (req, res) => res.status(200).send('OK'));

app.use(
	graphQlPath,
	cors(),
	bodyParser.json(),
	basicAuthenticationMiddleware,
	otherServicesAuthenticationMiddleware,
	i18nextMiddleware.handle(i18next),
);

const server = new ApolloServer({
	schema: graphQLSchema,
	introspection: true,
	uploads: {
    // Limits here should be stricter than config for surrounding
    // infrastructure such as Nginx so errors can be handled elegantly by
    // graphql-upload:
    // https://github.com/jaydenseric/graphql-upload#type-processrequestoptions
    maxFileSize: 5000000, // 50 MB
    maxFiles: 10,
  },
	context: ({ req }) => {
		return { req };
	}
});

server.applyMiddleware({ app, path: graphQlPath });

app.listen(process.env.PORT || 1338, () => {
	console.log(`Server listen on port ${process.env.PORT || 1337}`);
});
