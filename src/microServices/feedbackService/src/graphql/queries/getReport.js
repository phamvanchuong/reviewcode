import {ReportType, ReportListType} from '../types/report';
import {mutationOf, Primitives} from '../../../../../utils';
import {ReportModel} from "../../models/report";

export const getReport = {
    type: mutationOf(ReportType, 'ReportType1', ''),
    description: 'Get a report',
    args: {
        realEstateId: Primitives.string(),
    },
    resolve: async (rootValue, {realEstateId}, {req}) => {
        try {
            if(!realEstateId){
                return {
                    success: false,
                    msg: 'Not realEstateId!',
                }
            }
            const report = await ReportModel.findOne({realEstateId: realEstateId});
            console.log(report)
            return {
                success: true,
                msg: 'Get report successfully!',
                payload: report,
            }
        }catch (e) {
            return Promise.reject(e);
        }
    }
};

export const getReportList = {
    type: ReportListType,
    description: 'Get configuration',
    args: {
        page: Primitives.int(1),
        limit: Primitives.int(10),
    },
    resolve: async (rootValue, {page, limit}, {req}) => {
        try {
            if (page < 1) return Promise.reject(new Error('Page invalid'));
            const reportList = await ReportModel.find().sort({realEstateId: 1}).limit(limit).skip((page - 1) * limit);
            if (reportList.length < 1) return Promise.reject(new Error('No report remain'));
            const totalCount = await ReportModel.find().countDocuments(),
                totalPage = Math.ceil(totalCount/limit);
            console.log(reportList)
            return  {
                reports: reportList,
                page: page,
                limit: limit,
                totalCount: totalCount,
                totalPage: totalPage,
                hasNextPage: page<totalPage,
            };
        }catch (e) {
            return Promise.reject(e);
        }
    }
};
