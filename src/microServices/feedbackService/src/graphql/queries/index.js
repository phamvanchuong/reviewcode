import { GraphQLObjectType, GraphQLString as String, } from 'graphql';
import {getReport, getReportList} from './getReport'

export default new GraphQLObjectType({
	name: 'Queries',
	fields: () => ({
		[`${process.env.SERVICE_NAME}Greeting`]: greeting,
		getReport,
		getReportList,
	}),
});

const greeting = {
	type: String,
	description: 'A warm welcome message from GraphQL, usually used to Test if the system working..',
	resolve: (rootValue, params, context) => {
		const { req } = context;
		return req.t('greetingQuery', { service: process.env.SERVICE_NAME });
	},
};
