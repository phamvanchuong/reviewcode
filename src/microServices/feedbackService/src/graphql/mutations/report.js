import { Primitives } from '../../../../../utils';
import {ReportType} from '../types/report';
import {ReportModel} from '../../models/report';
import {mutationOf} from '../../../../../utils';
import { RealEstateReportReason } from '../../utils/helper';
import _ from 'lodash';

export const sendRealEstateFeedback = {
    type: mutationOf(ReportType, 'sendRealEstateFeedback','sen a feedback'),
    args: {
        realEstateId: Primitives.string(),
        reason: Primitives.string(),
        note: Primitives.string(),
    },
    resolve: async (rootValue, {realEstateId, reason, note}, context) => {
        try {
            const userId = (context.user)? context.user.userId : 'null';
            let data = {
                realEstateId,
                userId,
                reason,
                note,
            };
            const checkReason = RealEstateReportReason.filter((key) => key.key === reason);

            if(checkReason.length === 0) {
                return {
                    success: false,
                    msg: 'Reason key incorect!',
                }
            }

            if(checkReason[0].key !== 'Other'){
                data = _.omit(data, ['note']);
            }

            const newReport = new ReportModel (data);

            const report = await newReport.save();
            return {
                success: true,
                msg: 'Successfully',
                payload: report
            };
        } catch (e) {
            return Promise.reject(e);
        }
    }
}
