import { GraphQLObjectType } from 'graphql';
import {Resolvers} from "../../../../../utils";

export const ReportType = new GraphQLObjectType({
    name: 'ReportType',
    description: 'Report type',
    fields: () => ({
        realEstateId: Resolvers.string(),
        userId: Resolvers.string(),
        reason: Resolvers.string(),
        note: Resolvers.string(),
        createdAt: Resolvers.datetime(),
        deletedAt: Resolvers.datetime(),
    })
});

export const ReportListType = new GraphQLObjectType({
    name: 'ReportListType',
    description: 'Report list type',
    fields: () => ({
        reports: Resolvers.listOfType(ReportType, 'list report', reportList),
        page: Resolvers.int(),
        limit: Resolvers.int(),
        totalPage: Resolvers.int(),
        totalCount: Resolvers.int(),
        hasNextPage: Resolvers.boolean(),
    })
})

const reportList = async ({reports}) => {
    return reports;
}
