import mongoose, { Schema } from 'mongoose';

const reportsSchema = new Schema({
    realEstateId: String,
    userId: {type: String, default: null},
    reason: String,
    note: String,
    deletedAt: { type: Date, default: null },
}, {timestamps: true});

export const ReportModel = mongoose.model('Report', reportsSchema);


