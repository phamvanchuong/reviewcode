export const RealEstateReportReason = [
    {
        key: 'IncorrectInfo',
        value: 'Thông tin của tin rao không đúng',
    },
    {
        key: 'IncorrectImageOrVideo',
        value: 'Hình ảnh, video không đúng thực tế',
    },
    {
        key: 'IncorrectPrice',
        value: 'Giá cả thực tế không giống với tin rao',
    },
    {
        key: 'UnclearJuridical',
        value: 'Pháp lý không rõ ràng',
    },
    {
        key: 'IncorrectLocation',
        value: 'Tin đăng sai vị trí',
    },
    {
        key: 'IncorrectCategoryOrNeed',
        value: 'Sai danh mục hoặc nhu cầu',
    },
    {
        key: 'Other',
        value: ''
    },
]
