import { ApolloError } from 'apollo-server-express';

const CUSTOM_ERROR_CODE = 'CUSTOM_ERROR_CODE';

// Example custom error
export class CustomError extends ApolloError {
	constructor(message) {
		super(message, CUSTOM_ERROR_CODE);
	}
}
