import basicAuth from 'basic-auth';
import jwt from 'jsonwebtoken';
import { verifyTokenInUserService } from '../serviceCommunicationGraphQL/mutations';
import { redisClient } from '../redis';

export async function basicAuthenticationMiddleware(req, res, next) {
	try {
		const { headers: { authorization } } = req;
		if (!authorization) {
			return next()
		}
		const authUser = basicAuth(req);
		if (authUser && authUser.name === process.env.USERNAME && authUser.pass === process.env.PASSWORD ) {
			req.basicAuthenticated = true;
		}
		next();
	} catch (e) {
		return next()
	}
}

export async function otherServicesAuthenticationMiddleware(req, res, next) {
	try {
		const { headers: { authorization } } = req;
		let user;
		if (!authorization) {
			return next()
		}
		const accessToken = authorization.split(' ')[1];
		// Verify token
		const decoded = jwt.verify(accessToken, process.env.JWT_SECRET);
		if (!decoded) {
			return next();
		}
		const redisUser = await redisClient.get(accessToken);

		if (redisUser) {
			user = JSON.parse(redisUser);
		} else {
			const { loading, error, data: { verifyToken } } = await verifyTokenInUserService(accessToken);
			if (error) {
				return next()
			}

			if(!loading && verifyToken) {
				user = verifyToken;
			}
		}
		if(user) {
			Object.assign(req, {
				user: user,
				accessToken
			});
			return next();
		}
		return next()
	} catch (e) {
		return next()
	}
}

