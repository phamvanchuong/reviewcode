import { Tedis } from 'tedis';

export const redisClient = new Tedis({
	host: process.env.REDIS_URL || 'localhost',
	port: process.env.REDIS_PORT || 6379
});
