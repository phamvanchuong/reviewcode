import _ from 'lodash';
import bcrypt from 'bcryptjs';
import RoleModel from '../microServices/usersService/src/model/role';
import PermissionModel from '../microServices/usersService/src/model/permission';
import { UserModel } from '../microServices/usersService/src/model/user';

const permission = [
  { 
    name: "createProject",
    description: "Allow user to create a new project"
  },
  { 
    name: "getListOfProjects",
    description: "Allow user to get all projects"
  },
  { 
    name: "getListProjectsOfUser",
    description: "Allow user to get their own projects"
  },
  { 
    name: "updateProject",
    description: "Allow user to update projects"
  },
  { 
    name: "removeProject",
    description: "Allow user to remove specific project"
  },
  { 
    name: "addMemberToProject",
    description: "Allow user to add a new member to specific project"
  },
  { 
    name: "removeMemberOutOfProject",
    description: "Allow user to remove a member in project out of that project"
  },
  { 
    name: "assignWorksToSpecificMember",
    description: "Allow user to assign available works to specific user in that project"
  },
  { 
    name: "assignWorks",
    description: "Allow user to assign available works to all users in that project"
  },
  { 
    name: "verifyInvitation",
    description: "To verify the invitation to project is valid or not"
  },
  { 
    name: "getProjectDetails",
    description: "Allow user to get details of project"
  },
  { 
    name: "addImagesToProject",
    description: "Allow user to add images to project"
  },
  { 
    name: "removeImages",
    description: "Allow user to remove images out of project"
  },
  { 
    name: "getListOfImages",
    description: "Allow user to get all images of a project"
  },
  { 
    name: "getListImagesOfUser",
    description: "Allow user to get all their images of a project"
  },
  { 
    name: "addLabels",
    description: "Allow user to add labels to specific image"
  },
  { 
    name: "removeLabels",
    description: "Allow user to remove labels out of specific image"
  },
  { 
    name: "updateLabel",
    description: "Allow user to update a label of a specific image"
  },
  { 
    name: "getLabelsOfImage",
    description: "Allow user to get all labels of a specific image"
  },
  { 
    name: "getLabelsOfProject",
    description: "Allow user to get all labels of a specific project with class name and label count"
  },
  { 
    name: "create-user-admin",
    description: "Allow user to create user with admin role"
  },
  { 
    name: "update-user-admin",
    description: "Allow user to update user with admin role"
  },
  { 
    name: "view-user-admin",
    description: "Allow user to view admin user profile"
  },
  { 
    name: "delete-user-admin",
    description: "Allow user to delete admin user"
  },
  { 
    name: "create-user-supervisor",
    description: "Allow user to create user with supervisor role"
  },
  { 
    name: "update-user-supervisor",
    description: "Allow user to update supervisor role user"
  },
  { 
    name: "view-user-supervisor",
    description: "Allow user to get supervisor user role"
  },
  { 
    name: "delete-user-supervisor",
    description: "Allow user to delete supervisor user"
  },
  { 
    name: "create-user-employee",
    description: "Allow user to create user with employee role"
  },
  { 
    name: "update-user-employee",
    description: "Allow user to update employee role user"
  },
  { 
    name: "view-user-employee",
    description: "Allow user to get employee user role"
  },
  { 
    name: "delete-user-employee",
    description: "Allow user to delete employee user"
  },
],
role = [
  {
    name: "admin"
  },
  {
    name: "supervisor"
  },
  {
    name: "employee"
  }
];

export const initialize = async () => {
  
  
  console.log('Creating permissions and roles ...')
  const createPermissions = await PermissionModel.insertMany(permission),
  // create roles
  createRoles = await RoleModel.insertMany(role),
  adminRole =  await RoleModel.findOne({ name: 'admin' });
  console.log(`${createPermissions.length} permissions created. \n ${createRoles.length} roles created.`)
  adminRole.permissions = _.map(createPermissions, '_id');
  await adminRole.save();
  console.log(`admin role was set with these permissions \n ${_.map(adminRole.permissions, 'name')}`)
  console.log('======= Creating admin account ======');
  
  const hash = bcrypt.hashSync('1234567Ocr@', 10),
  newUser = await new UserModel({
    firstName: "Admin",
    lastName: "User",
    email: "admin@example.com",
    password: hash,
    isPasswordCreated: true
  }).save();

  return { message: `Admin user with email ${newUser.email} was created. Using password 1234567Ocr@ to login.`};
}
