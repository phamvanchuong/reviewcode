import compress from 'compress-images';

export const compressImage = (image_path, ouput_path='') => {
  return compress(image_path, '../../output/img', { compress_force: false, statistic: true, autoupdate: true }, false,
    {jpg: {engine: 'mozjpeg', command: ['-quality', '60']}},
    { png: { engine: 'pngquant', command: ['--quality=20-50'] } },
    { svg: { engine: 'svgo', command: '--multipass' } },
    { gif: { engine: 'gifsicle', command: ['--colors', '64', '--use-col=web'] } });
}
