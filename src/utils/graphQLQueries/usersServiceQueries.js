import gql from 'graphql-tag';

export const getUserById = gql`
	query GetUserInfo($id: String!){
		getUserInfo(id: $id){
			id
			firstName
			lastName
			email
			turnOnNotification
			deviceToken
		}
	}
`;

export const getUsersByConditions = gql`
	query GetUsersByConditions($conditions: UserCondition!){
		getListOfUsers(conditions: $conditions){
			id
			firstName
			lastName
			email
			turnOnNotification
			deviceToken
		}
	}
`;

export const checkUserPermission = gql`
	query CheckPermission($userId: String!, $action: String!){
		checkPermission(userId: $userId, action: $action){
			has
		}
	}
`;