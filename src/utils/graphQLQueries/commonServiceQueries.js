import gql from 'graphql-tag';

export const commonGreeting = gql`
	query CommonGreeting {
		commonGreeting
	}
`;

export const commonSendSMS = gql`
	mutation SendSMS($phone: String!, $message: String!) {
		sendSMS(phone: $phone, message: $message){
			message
		}
	}
`;

export const commonSendMail = gql`
	mutation SendEmail($email: String!, $mailSubject: String!, $mailTemplate: String!, $variables: String, $attach: String) {
		sendEmail(email: $email, mailSubject: $mailSubject, mailTemplate: $mailTemplate, variables: $variables, attach: $attach){
			message
		}
	}
`;