import gql from 'graphql-tag';

export const verifyToken = gql`
mutation VerifyToken($token: String!){
	verifyToken(token: $token){
		id
		firstName
		lastName
		email
		turnOnNotification
		deviceToken
		createdAt
		updatedAt
	}
}
`;