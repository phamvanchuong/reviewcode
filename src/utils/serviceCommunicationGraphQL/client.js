import ApolloClient from 'apollo-boost';
import fetch from 'node-fetch';

const isProduction = process.env.NODE_ENV === 'production',
	baseUri = {
		users: `${process.env.USER_SERVICE_HOST}:${process.env.USER_SERVICE_PORT}`,
		common: `${process.env.COMMON_SERVICE_HOST}:${process.env.COMMON_SERVICE_PORT}`,
		image: `${process.env.IMAGE_SERVICE_HOST}:${process.env.IMAGE_SERVICE_PORT}`
	},
	getUri = (serviceName) => `${isProduction ? process.env.BASE_URL : baseUri[serviceName]}/${serviceName}/graphql`;

function encodeBase64(username, password) {
	return Buffer.from(`${username}:${password}`).toString('base64');
}

export const getUsersServiceClient = () => {
	if (!process.env.USER_SERVICE || !process.env.USER_SERVICE_PASSWORD) {
		return null;
	}
	const token = encodeBase64(process.env.USER_SERVICE, process.env.USER_SERVICE_PASSWORD);
	return new ApolloClient({
		uri: getUri('users'),
		headers: {
			'Authorization': 'Basic ' + token
		},
		fetch
	});
};

export const getCommonServiceClient = () => {
	if (!process.env.COMMON_SERVICE || !process.env.COMMON_SERVICE_PASSWORD) {
		return null;
	}
	const token = encodeBase64(process.env.COMMON_SERVICE, process.env.COMMON_SERVICE_PASSWORD);
	return new ApolloClient({
		uri: getUri('common'),
		headers: {
			'Authorization': 'Basic ' + token
		},
		fetch
	});
};

export const getImageServiceClient = () => {
	if (!process.env.IMAGE_SERVICE || !process.env.IMAGE_SERVICE_PASSWORD) {
		return null;
	}
	const token = encodeBase64(process.env.IMAGE_SERVICE, process.env.IMAGE_SERVICE_PASSWORD);
	return new ApolloClient({
		uri: getUri('image'),
		headers: {
			'Authorization': 'Basic ' + token
		},
		fetch
	});
};