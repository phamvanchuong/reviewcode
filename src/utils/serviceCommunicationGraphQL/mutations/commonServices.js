import { getCommonServiceClient } from '../client';
import { commonSendSMS, commonSendMail } from '../../graphQLQueries';

const client = getCommonServiceClient();

export function sendSMS(phone, message) {
	return client.mutate({
		mutation: commonSendSMS,
		variables: { phone: phone, message: message }
	});
}

export function sendEmail(email, mailSubject, mailTemplate, variables, attach) {
	return client.mutate({
		mutation: commonSendMail,
		variables: { email: email, mailSubject: mailSubject, mailTemplate: mailTemplate, variables: variables, attach: attach }
	});
}