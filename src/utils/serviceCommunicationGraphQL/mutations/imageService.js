import { getImageServiceClient } from '../client';
import { removeImages } from '../../graphQLMutations';

const client = getImageServiceClient();

export function removeImageFromS3(imageKeys) {
	return client.mutate({
		mutation: removeImages,
		variables: { imageKeys: imageKeys }
	});
}
