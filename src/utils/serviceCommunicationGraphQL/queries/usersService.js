import { getUserById, getUsersByConditions, checkUserPermission } from '../../graphQLQueries';
import { getUsersServiceClient } from '../client';

const client = getUsersServiceClient();

export const getUsersInfoByConditions = (conditions) => {
	return client.query({
		query: getUsersByConditions,
		variables: { conditions }
	})
};

export const getUserInfoById = (id) => {
	return client.query({
		query: getUserById,
		variables: { id }
	})
};

export const checkPermissionOfUser = (userId, action) => {
	return client.query({
		query: checkUserPermission,
		variables: { userId, action }
	})
};