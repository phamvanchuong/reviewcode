import i18next from 'i18next';
import i18nextMiddleware from 'i18next-express-middleware';

import localeEN from './en';
import localeVN from './vi';

i18next.use(i18nextMiddleware.LanguageDetector).init({
	detection: {
		order: ['header'],
		lookupHeader: 'accept-language'
	},
	preload: ['en', 'vi'],
	whitelist: ['en', 'vi'],
	fallbackLng: 'en',
	resources: {
		en: { translation: localeEN },
		vi: { translation: localeVN }
	}
});

module.exports = {
	i18next,
	i18nextMiddleware
};
