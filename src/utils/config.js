

export const userType = {
  homeOwner: { id: '1', en: 'HomeOwner', vi: 'Chủ nhà' },
  broker: { id: '2', en: 'Broker', vi: 'Môi giới' },
  company: { id: '3', en: 'Company', vi: 'Doanh nghiệp' },
}
export const regions = {
  vn: { phoneCode: '+84' },
  us: { phoneCode: '+1' }
}

export const realEstatePostType = [
  { id: '1', nameEn: 'Normal', nameVi: 'Tin thường' },
  { id: '2', nameEn: 'VIP 1', nameVi: 'Tin VIP 1' },
  { id: '1', nameEn: 'Special VIP', nameVi: 'Tin VIP Đặc biệt' },
]


export const province = [
  { id: 1, name: 'Hồ Chí Minh', shortname: 'HCM', pos: 1},
  { id: 2, name: 'Hà Nội', shortname: 'HN', pos: 2},
  { id: 3, name: 'Đà Nẵng', shortname: 'DN', pos: 3}
]

export const legal = {
  house: ['Sổ hồng', 'Sổ đỏ', 'Bằng khoán', 'Đang thế chấp'],
  apartment: ['Sổ hồng', 'Ngân hàng chấp thuận cho vay', 'Đang chờ bàn giao sổ'],
  shophouse: ['Sổ hồng', 'Ngân hàng chấp thuận cho vay', 'Đang chờ bàn giao sổ'],
  villa: ['Sổ hồng', 'Sổ đỏ', 'Bằng khoán', 'Đang thế chấp', 'Đang chờ bàn giao sổ'],
  penthouse: ['Sổ hồng', 'Ngân hàng chấp thuận cho vay', 'Đang chờ bàn giao sổ'],
  land: ['Sổ hồng', 'Sổ đỏ', 'Bằng khoán', 'Đang thế chấp', 'Đang chờ bàn giao sổ'],
}

export const warehouseType = ["Xưởng công nghiệp", "Xưởng nông nghiệp", "Xưởng thực phẩm", "Xưởng gia công", "Xưởng sản xuất", "Xưởng kho"]