import nodemailer from 'nodemailer';
import handlebars from 'handlebars';
import { readFileSync, createWriteStream, unlink } from 'fs';
import path from 'path';
import shortid from 'shortid';
import { regions } from './config';

/**
 * Return a unique identifier with the given `len`.
 *
 *     utils.uid(10);
 *     // => "FDaS435D2z"
 *
 * @param {Number} len
 * @return {String}
 * @api private
 */

export function uid(len) {
    const buf = [],
        chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789',
        charlen = chars.length;

    for (let i = 0; i < len; ++i) {
        buf.push(chars[getRandomInt(0, charlen - 1)]);
    }

    return buf.join('');
}

export function random6digits() {
    return Math.floor(100000 + Math.random() * 900000)
}

export function uidLight(len) {
    const buf = [],
        chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghiklmnopqrstuvwxyz0123456789',
        charlen = chars.length;

    for (let i = 0; i < len; ++i) {
        buf.push(chars[getRandomInt(0, charlen - 1)]);
    }
    return buf.join('');
}

/**
 * Return a random int, used by `utils.uid()`
 *
 * @param {Number} min
 * @param {Number} max
 * @return {Number}
 * @api private
 */

export function getRandomInt(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

/**
 * Get HTML email template
 *
 * see: https://www.npmjs.com/package/handlebars
 */
export function get_header_email_template(variables) {
    const data = {
        headerText1: variables.headerText1 ? variables.headerText1 : 'Welcome to OCR',
        headerText2: variables.headerText2 ? variables.headerText2 : 'powered SalemSolutions',
        content: '{{content}}'
    };
    const html = readFileSync(path.join(__dirname, '../microServices/commonService/src/template/email') + '/global-template.html', {
        encoding: 'utf-8'
    });
    return handlebars.compile(html)(data);
}

export function getHtmlFromTemplate(htmlTemplate, variables) {
    return new Promise(async (resolve) => {
        try {
            let newHtml = get_header_email_template(variables);
            const html = readFileSync(path.join(__dirname, '../microServices/commonService/src/template/email') + '/' + htmlTemplate + '-content.html', { encoding: 'utf-8' }),
                template = handlebars.compile(html),
                result = template(variables);
            newHtml = newHtml.replace('{{content}}', result);
            resolve(newHtml);
        } catch (error) {
            resolve(null);
        }
    });

}

export async function sendMailX(mailTo, subject, htmlTemplate, variables, attach) {

    let transporter,
        attachFile = attach || null;


    // create reusable transporter object using the default SMTP transport
    transporter = nodemailer.createTransport({
        service: 'Zoho',
        host: "smtp.zoho.com",
        port: 587,
        secure: false, // true for 465, false for other ports
        auth: {
            user: email, // generated ethereal user
            pass: email_password // generated ethereal password
        }
    });

    let html = await getHtmlFromTemplate(htmlTemplate, variables);

    let mailOptions;
    if (attachFile) {
        mailOptions = {
            from: email, // email
            to: mailTo,
            subject: subject,
            html: html,
            attachments: [{ // stream as an attachment
                filename: attachFile.fileName,
                content: new Buffer(attachFile.data, 'utf-8')
            }]
        };
    } else {
        mailOptions = {
            from: email, // email
            to: mailTo,
            subject: subject,
            html: html
        };
    }

    return transporter.sendMail(mailOptions);

}

export async function sendMailZoho(mailTo, subject, htmlTemplate, variables, attach) {
    const transporter = nodemailer.createTransport({
        service: 'Google',
        host: 'smtp.gmail.com',
        port: 587,
        secure: false, // true for 465, false for other ports
        auth: {
            user: process.env.EMAIL, // generated ethereal user
            pass: process.env.EMAIL_PASSWORD // generated ethereal password
        }
    }),
        attachFile = attach || null,
        html = await getHtmlFromTemplate(htmlTemplate, variables);
    const mailOptions = {};
    if (attachFile) {
        Object.assign(mailOptions, {
            from: `${process.env.APP_NAME} ${process.env.EMAIL}`, // email
            to: mailTo,
            subject: subject,
            html: html,
            attachments: [{ // stream as an attachment
                filename: attachFile.fileName,
                content: new Buffer(attachFile.data, 'utf-8')
            }]
        });
    } else {
        Object.assign(mailOptions, {
            from: `"Salemsolutions" ${process.env.EMAIL}`, // email
            to: mailTo,
            subject: subject,
            html: html
        });
    }
    return transporter.sendMail(mailOptions);
}

export const storeUploadFile = async (upload, req) => {
    const UPLOAD_DIR = '../assets/uploads',
        { createReadStream, filename, mimetype } = await upload,
        stream = createReadStream(),
        id = shortid.generate(),
        filePath = path.join(__dirname, `${UPLOAD_DIR}/${id}-${filename}`),
        file = { id, filename, mimetype, filePath };
    // Store the file in the filesystem.
    await new Promise((resolve, reject) => {

        if (mimetype !== 'text/plain') {
            return reject(new Error(req.t('onlyTxtSupport')));
          }
        // Create a stream to which the upload will be written.
        const writeStream = createWriteStream(filePath);

        // When the upload is fully written, resolve the promise.
        writeStream.on('finish', resolve);

        // If there's an error writing the file, remove the partially written file
        // and reject the promise.
        writeStream.on('error', (error) => {
            unlink(filePath, () => {
                reject(error);
            });
        });

        // In node <= 13, errors are not automatically propagated between piped
        // streams. If there is an error receiving the upload, destroy the write
        // stream with the corresponding error.
        stream.on('error', (error) => writeStream.destroy(error));

        // Pipe the upload into the write stream.
        stream.pipe(writeStream);
    });

    return file;
}

export const getMediaRedisKey = (mediaId) => `media:${mediaId}`;

/**
 *
 * @param {String} phone
 * Valid format:
 * (123) 456-7890
 * (123)456-7890
 * 123-456-7890
 * 123.456.7890
 * 1234567890
 * +31636363634
 */
export const phoneValidator = (phone) => {
    const regex = /^[+]?[(]?[0-9]{3}[)]?[-\s.]?[0-9]{3}[-\s.]?[0-9]{4,6}$/
    return regex.test(phone);
}

export const addRegionPhoneCode = (region = 'vn', phone) => {
    return regions[region]?.phoneCode + phone.substring(1);
}
