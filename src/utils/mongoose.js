import mongoose from 'mongoose';

const DB_HOST = process.env.DB_HOST || 'localhost',
	DB_PORT = process.env.DB_PORT || '27017',
	DB_NAME = process.env.DB_NAME || `${process.env.SERVICE_NAME}_service`,
	DB_USERNAME = process.env.DB_USERNAME || '',
	DB_PASSWORD = process.env.DB_PASSWORD || '',
	LOGIN_DB = DB_USERNAME && DB_PASSWORD ? `${DB_USERNAME}:${DB_PASSWORD}@` : '';

mongoose
	.connect(`mongodb${LOGIN_DB ? '+svr' : ''}://${LOGIN_DB}${DB_HOST}:${DB_PORT}/${DB_NAME}`, {useNewUrlParser: true, useUnifiedTopology: true, connectTimeoutMS: 5000})
	.catch(console.log);

mongoose.connection.on('open', () => console.log('Mongo DB connected'));
